@extends('layouts.app')

@section('title', 'Profile')

@section('breadcrumb')
<li class="breadcrumb-item">Settings</li>
<li class="breadcrumb-item active">Profile</li>
@endsection

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="profile-details text-center">
                        <img src="{{ asset('images/user/' . Auth::user()->avatar) }}" alt=""
                            class="img-fluid img-90 rounded-circle blur-up lazyloaded">
                        <h5 class="f-w-600 mb-0">{{ auth()->user()->name }}</h5>
                        <span>{{ auth()->user()->email }}</span>
                        <div class="social">
                            <div class="form-group btn-showcase">
                                <a href="https://facebook.com/{{ $attribute->facebook }}"
                                    class="btn social-btn btn-fb d-inline-block"> <i class="fa fa-facebook"></i></a>
                                <a href="https://instagram.com/{{ $attribute->instagram }}"
                                    class="btn social-btn btn-google d-inline-block"><i class="fa fa-instagram"></i></a>
                                <a href="https://twitter.com/{{ $attribute->twitter }}"
                                    class="btn social-btn btn-twitter d-inline-block mr-0"><i
                                        class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card tab2-card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="top-profile-tab" data-toggle="tab"
                                href="#top-profile" role="tab" aria-controls="top-profile" aria-selected="true"><i
                                    data-feather="user" class="mr-2"></i>Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" id="contact-top-tab" data-toggle="tab"
                                href="#top-contact" role="tab" aria-controls="top-contact" aria-selected="false"><i
                                    data-feather="settings" class="mr-2"></i>Contact</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="top-tabContent">
                        <div class="tab-pane fade show active" id="top-profile" role="tabpanel"
                            aria-labelledby="top-profile-tab">
                            <h5 class="f-w-600 mb-4">Profile</h5>
                            <div class="form-group row">
                                <label class="col-3">Name</label>
                                <label class="col-9">{{ auth()->user()->name }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Username</label>
                                <label class="col-9">{{ auth()->user()->username }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Email</label>
                                <label class="col-9">{{ auth()->user()->email }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Gender</label>
                                <label class="col-9">{{ $attribute->gender }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">DOB</label>
                                <label class="col-9">{{ $dob }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Phone</label>
                                <label class="col-9">{{ $attribute->phone }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Address</label>
                                <label class="col-9">{{ $attribute->address }}</label>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
                            <div class="account-setting deactivate-account">
                                <h5 class="f-w-600">Deactivate Account</h5>
                                <div class="row">
                                    <div class="col">
                                        <label class="d-block" for="edo-ani">
                                            <input class="radio_animated" id="edo-ani" type="radio" name="rdo-ani"
                                                checked="">
                                            I have a privacy concern
                                        </label>
                                        <label class="d-block" for="edo-ani1">
                                            <input class="radio_animated" id="edo-ani1" type="radio" name="rdo-ani">
                                            This is temporary
                                        </label>
                                        <label class="d-block mb-0" for="edo-ani2">
                                            <input class="radio_animated" id="edo-ani2" type="radio" name="rdo-ani"
                                                checked="">
                                            Other
                                        </label>
                                    </div>
                                </div>
                                <a href="{{ route('profile.inactive') }}" class="btn btn-primary">Deactivate Account</a>
                            </div>
                            <div class="account-setting deactivate-account">
                                <h5 class="f-w-600">Delete Account</h5>
                                <div class="row">
                                    <div class="col">
                                        <label class="d-block" for="edo-ani3">
                                            <input class="radio_animated" id="edo-ani3" type="radio" name="rdo-ani1"
                                                checked="">
                                            No longer usable
                                        </label>
                                        <label class="d-block" for="edo-ani4">
                                            <input class="radio_animated" id="edo-ani4" type="radio" name="rdo-ani1">
                                            Want to switch on other account
                                        </label>
                                        <label class="d-block mb-0" for="edo-ani5">
                                            <input class="radio_animated" id="edo-ani5" type="radio" name="rdo-ani1"
                                                checked="">
                                            Other
                                        </label>
                                    </div>
                                </div>
                                <a href="{{ route('profile.delete') }}" class="btn btn-primary">Delete Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
