@extends('layouts.app')

@section('title', 'Create Category')

@section('breadcrumb')
<li class="breadcrumb-item">Category</li>
<li class="breadcrumb-item active">Create</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}}}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Category</h5>
                    <a href="{{ route('categories.index') }}" class="btn btn-sm btn-default float-right"><i
                            class="fa fa-arrow-left"></i> Back</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                        @csrf
                        @method('POST')

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Name :</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        value="{{ old('name') }}" name="name" id="name" required>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="parent" class="col-form-label">Parent :</label>
                                    <select class="form-control" name="parent" id="parent">
                                        <option value=""> No Parent</option>
                                        @foreach ($parents as $parent)
                                        <option value="{{ $parent->id }}"> -&nbsp;{{ $parent->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="image" class="col-form-label">Image :</label>
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                        class="form-control @error('image') is-invalid @enderror"
                                        value="{{ old('image') }}" name="image" id="image">
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
