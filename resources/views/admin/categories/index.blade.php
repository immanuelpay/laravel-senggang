@extends('layouts.app')

@section('title', 'Manage Categories')

@section('breadcrumb')
<li class="breadcrumb-item active">Categories</li>
@endsection

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('categories.index') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'name'},
                {data: 'parentID'},
                {data: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Manage Categories</h5>
                    <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary float-right"><i
                            class="fa fa-plus"></i> Add
                        Category</a>
                    <a href="{{ route('categories.trash') }}" class="btn btn-sm btn-danger float-right mr-3"><i
                            class="fa fa-recycle"></i>
                        Trash Category</a>
                    <button type="button" onclick="$('#datatable').DataTable().ajax.reload();"
                        class="btn btn-sm btn-success float-right mr-3"><i class="fa fa-refresh"></i>
                        Refresh</button>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>Name</th>
                                    <th>Parent</th>
                                    <th width="95" align="center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
