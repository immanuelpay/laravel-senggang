@extends('layouts.app')

@section('title', 'Edit Category')

@section('breadcrumb')
<li class="breadcrumb-item">Category</li>
<li class="breadcrumb-item active">Edit</li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Category : <b> {{ $category->name }}</b></h5>
                    <a href="{{ route('categories.index') }}" class="btn btn-sm btn-default float-right"
                        id="button-back"><i class="fa fa-arrow-left"></i>
                        Back</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('categories.update', ['category' => $category->id]) }}"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Name :</label>
                                    <input type="text" class="form-control" value="{{ $category->name }}" name="name"
                                        id="name" required>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="parent" class="col-form-label">Parent :</label>
                                    <select class="form-control" name="parent" id="parent">
                                        <option value=""> No Parent</option>
                                        @foreach ($parents as $parent)
                                        <option value="{{ $parent->id }}"
                                            {{ $parent->id == $category->parentID ? 'selected' : '' }}>
                                            -&nbsp;{{ $parent->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <img src="{{ asset('images/category/medium/'.$category->image) }}"
                                    class="img-thumbnail mx-auto d-block" align="center" width="120px" />
                                <div class="form-group">
                                    <label for="image" class="col-form-label">Image :</label>
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        value="{{ $category->image }}" name="image" id="image">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
