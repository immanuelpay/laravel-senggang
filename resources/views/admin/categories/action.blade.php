@if (request()->is('categories'))
<a class="btn-sm btn-info mr-1" href="#modalShow" data-toggle="modal" data-target="#modalShow" onclick="
    $('#modalShow .categoryImage').attr('src', '{{ url('/images/category/medium'. '/'. $request->image) }}');
    $('#modalShow #modalShowLabel').text('Show Category : {{ $request->name }}');
    $('#modalShow #bodyShow .categoryName').text('{{ $request->name }}');
    $('#modalShow #bodyShow .categorySlug').text('{{ $request->slug }}');
    $('#modalShow #bodyShow .categoryParent').text('{{ $request->parentID == 0 ? '-' : $request->parent->name }}');">
    <i class="fa fa-eye text-inverse"></i>
</a>
<a href="{{ $url_edit }}" class="btn-sm btn-warning"><i class="fa fa-pencil text-inverse"></i></a>
<a class="btn-sm btn-danger" href="#modalDelete" data-toggle="modal" data-target="#modalDelete" onclick="
    $('#modalDelete #linkDelete').attr('href', '{{ $url_delete }}');
    $('#modalDelete #modalDeleteLabel').text('Are you sure?');
    $('#modalDelete #bodyDelete #deleteTitle').text('You want to moved the ');
    $('#modalDelete #bodyDelete #deleteName').text('{{ $request->name }}');
    $('#modalDelete #bodyDelete #deleteTitle1').text(' category to trash?');">
    <i class="fa fa-trash text-inverse"></i>
</a>
@endif

@if (request()->is('categories/trash'))
<a href="{{ $url_restore }}" class="btn-sm btn-warning"><i class="fa fa-refresh text-inverse"></i></a>
<a class="btn-sm btn-danger" href="#" data-toggle="modal" data-target="#modalDelete" onclick="
    $('#modalDelete #linkDelete').attr('href', '{{ $url_delete_permanent }}');
    $('#modalDelete #modalDeleteLabel').text('Are you sure?');
    $('#modalDelete #bodyDelete #deleteTitle').text('You want to permanently deleted the ');
    $('#modalDelete #bodyDelete #deleteName').text('{{ $request->name }}');
    $('#modalDelete #bodyDelete #deleteTitle1').text(' category?');">
    <i class="fa fa-trash text-inverse"></i>
</a>
@endif
