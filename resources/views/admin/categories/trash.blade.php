@extends('layouts.app')

@section('title', 'Trash Categories')

@section('breadcrumb')
<li class="breadcrumb-item">Categories</li>
<li class="breadcrumb-item active">Trash</li>
@endsection

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('categories.trash') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'name'},
                {data: 'parentID'},
                {data: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Trash Categories</h5>
                    <a href="{{ route('categories.index') }}" class="btn btn-sm btn-default float-right"><i
                            class="fa fa-arrow-left"></i>
                        Back</a>
                </div>

                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>Name</th>
                                    <th>Parent</th>
                                    <th width="58" align="center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
