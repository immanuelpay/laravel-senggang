<a href="{{ $url_show }}" class="btn-sm btn-info"><i class="fa fa-eye text-inverse"></i></a>
<a href="{{ $url_edit }}" class="btn-sm btn-warning"><i class="fa fa-pencil text-inverse"></i></a>
<a href="#modalDelete" class="btn-sm btn-danger" data-toggle="modal" data-target="#modalDelete" onclick="
    $('#modalDelete #linkDelete').attr('href', '{{ $url_delete }}');
    $('#modalDelete #modalDeleteLabel').text('Are you sure?');
    $('#modalDelete #bodyDelete #deleteTitle').text('You want to permanently deleted the ');
    $('#modalDelete #bodyDelete #deleteName').text('{{ $request->name }}');
    $('#modalDelete #bodyDelete #deleteTitle1').text(' user?');">
    <i class="fa fa-trash text-inverse"></i>
</a>
