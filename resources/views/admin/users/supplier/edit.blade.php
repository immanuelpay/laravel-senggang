@extends('layouts.app')

@section('title', 'Create Supplier')

@section('breadcrumb')
<li class="breadcrumb-item">Supplier</li>
<li class="breadcrumb-item active">Edit</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}}}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
@endpush

@section('content')
<form method="POST" action="{{ route('user.supplier.update', $supplier->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="container-fluid">
        <div class="product-adding">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Supplier Info : <b>{{ $company->companyName }}</b></h5>
                </div>
                <div class="card-body">
                    <div class="digital-add needs-validation">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="companyName" class="col-form-label"><span>*</span> Company Name</label>
                                    <input class="form-control" name="companyName" id="companyName" type="text"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="contactFName" class="col-form-label"><span>*</span> Contact First
                                        Name</label>
                                    <input class="form-control" name="contactFName" id="contactFName" type="text"
                                        required="">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="contactLName" class="col-form-label"><span>*</span> Contact Last
                                        Name</label>
                                    <input class="form-control" name="contactLName" id="contactLName" type="text"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-7">
                                <div class="form-group">
                                    <label for="contactTitle" class="col-form-label"><span>*</span> Contact
                                        Title</label>
                                    <input class="form-control" name="contactTitle" id="contactTitle" type="text"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="address1" class="col-form-label"><span>*</span> Address 1</label>
                                    <textarea class="form-control" name="address1" id="address1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="address2" class="col-form-label"><span>*</span> Address 2</label>
                                    <textarea class="form-control" name="address2" id="address2" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="city" class="col-form-label"><span>*</span> City</label>
                                    <select class="form-control" name="city" id="city">
                                        <option>--Select City--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="state" class="col-form-label"><span>*</span> State</label>
                                    <select class="form-control" name="state" id="state">
                                        <option>--Select State--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="postalCode" class="col-form-label"><span>*</span> Postal Code</label>
                                    <select class="form-control" name="postalCode" id="postalCode">
                                        <option>--Select Postal Code--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="country" class="col-form-label"><span>*</span> Country</label>
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Country--</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="phone" class="col-form-label"><span>*</span> Phone</label>
                                    <input class="form-control" name="phone" id="phone" type="text" required="">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="fax" class="col-form-label"><span>*</span> Fax</label>
                                    <input class="form-control" name="fax" id="fax" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="email" class="col-form-label"><span>*</span> Email</label>
                                    <input class="form-control" name="email" id="email" type="email" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="facebook" class="col-form-label"><span>*</span> Facebook</label>
                                    <input class="form-control" name="facebook" id="facebook" type="text" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="instagram" class="col-form-label"><span>*</span> Instagram</label>
                                    <input class="form-control" name="instagram" id="instagram" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="twitter" class="col-form-label"><span>*</span> Twitter</label>
                                    <input class="form-control" name="twitter" id="twitter" type="text" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="linkedin" class="col-form-label"><span>*</span> LinkedIn</label>
                                    <input class="form-control" name="linkedin" id="linkedin" type="text" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="websitepaymentMethods" class="col-form-label"><span>*</span>
                                        Website</label>
                                    <input class="form-control" name="websitepaymentMethods" id="websitepaymentMethods"
                                        type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="paymentMethods" class="col-form-label"><span>*</span> Payment
                                        Methods</label>
                                    <select class="form-control" name="paymentMethods" id="paymentMethods">
                                        <option>--Select Payment Methods--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="discountType" class="col-form-label"><span>*</span> Discount
                                        Type</label>
                                    <select class="form-control" name="discountType" id="discountType">
                                        <option>--Select Discount Type--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="discountRate" class="col-form-label"><span>*</span> Discount Rate
                                        (%)</label>
                                    <input class="form-control" name="stock" id="discountRate" type="discountRate"
                                        required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="picture" class="col-form-label"><span>*</span> Picture</label>
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="picture" id="picture">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row product-adding">

            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Supplier Account</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Username</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="email" class="col-4 col-form-label">Email</label>
                                <div class="col-8">
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password" class="col-4 col-form-label">Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password_confirmation" class="col-4 col-form-label">Confirm
                                    Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password_confirmation"
                                        name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Status</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="ACTIVE">
                                        <input class="radio_animated" id="ACTIVE" type="radio" name="status"
                                            value="ACTIVE">
                                        Active
                                    </label>
                                    <label class="d-block" for="INACTIVE">
                                        <input class="radio_animated" id="INACTIVE" type="radio" name="status"
                                            value="INACTIVE">
                                        Inactive
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">Avatar</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>Change Password</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="password" class="col-4 col-form-label">New Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password_confirmation" class="col-4 col-form-label">Confirm Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password_confirmation"
                                        name="password_confirmation">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Identity Image</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="image1" class="col-4 col-form-label">KTP</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="image1" id="image1">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="image2" class="col-4 col-form-label">KK</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="image2" id="image2">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="image3" class="col-4 col-form-label">Business License</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="image3" id="image3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>Edit Supplier Attribute</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="dob" class="col-4 col-form-label">DOB</label>
                                <div class="col-8">
                                    <input type="date" class="form-control" id="dob" name="dob">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Gender</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="Male">
                                        <input class="radio_animated" id="Male" type="radio" name="gender" value="Male">
                                        Male
                                    </label>
                                    <label class="d-block" for="Female">
                                        <input class="radio_animated" id="Female" type="radio" name="gender"
                                            value="Female">
                                        Female
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="address" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address" id="address" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="phone" class="col-4 col-form-label">Phone</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="facebook" class="col-4 col-form-label">Facebook</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="facebook" name="facebook">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="instagram" class="col-4 col-form-label">Instagram</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="instagram" name="instagram">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="twitter" class="col-4 col-form-label">Twitter</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="twitter" name="twitter">
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <a href="{{ route('user.supplier.index') }}" class="btn btn-light"><i
                                        class="fa fa-arrow-left"></i>
                                    Back</a>
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>
                                    Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Container-fluid Ends-->
@endsection
