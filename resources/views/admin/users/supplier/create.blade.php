@extends('layouts.app')

@section('title', 'Create Supplier')

@section('breadcrumb')
<li class="breadcrumb-item">Supplier</li>
<li class="breadcrumb-item active">Create</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}}}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
@endpush

@section('content')
<form method="POST" action="{{ route('user.supplier.store') }}" enctype="multipart/form-data">
    @csrf

    <div class="container-fluid">
        <div class="product-adding">
            <div class="card">
                <div class="card-header">
                    <h5>Add Supplier Info</h5>
                </div>
                <div class="card-body">
                    <div class="digital-add needs-validation">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="companyName" class="col-form-label"><span>*</span> Company Name</label>
                                    <input class="form-control" name="companyName" id="companyName" type="text">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="companyEmail" class="col-form-label"><span>*</span> Company
                                        Email</label>
                                    <input type="email" class="form-control" name="companyEmail" id="companyEmail">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row product-adding">

            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Supplier Account</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Username</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="email" class="col-4 col-form-label">Email</label>
                                <div class="col-8">
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password" class="col-4 col-form-label">Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password_confirmation" class="col-4 col-form-label">Confirm
                                    Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password_confirmation"
                                        name="password_confirmation">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">Avatar</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Supplier Attribute</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="dob" class="col-4 col-form-label">DOB</label>
                                <div class="col-8">
                                    <input type="date" class="form-control" id="dob" name="dob">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Gender</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="Male">
                                        <input class="radio_animated" id="Male" type="radio" name="gender" value="Male">
                                        Male
                                    </label>
                                    <label class="d-block" for="Female">
                                        <input class="radio_animated" id="Female" type="radio" name="gender"
                                            value="Female">
                                        Female
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="address" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address" id="address" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="phone" class="col-4 col-form-label">Phone</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="facebook" class="col-4 col-form-label">Facebook</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="facebook" name="facebook">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="instagram" class="col-4 col-form-label">Instagram</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="instagram" name="instagram">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="twitter" class="col-4 col-form-label">Twitter</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="twitter" name="twitter">
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <a href="{{ route('user.supplier.index') }}" class="btn btn-light"><i
                                        class="fa fa-arrow-left"></i>
                                    Back</a>
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>
                                    Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Container-fluid Ends-->
@endsection
