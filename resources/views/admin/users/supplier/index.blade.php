@extends('layouts.app')

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('title', 'Manage Suppliers')

@section('breadcrumb')
<li class="breadcrumb-item active">Suppliers</li>
@endsection

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('user.supplier.index') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'companyName'},
                {data: 'name'},
                {data: 'email'},
                {data: 'roles'},
                {
                    data: 'status',
                    render: function (data, type, full, meta)
                    {
                        if (data == "INACTIVE") {
                        return '<span class="badge badge-danger">'+ data +'</span>';
                        } else {
                        return '<span class="badge badge-success">'+ data +'</span>';
                        }
                    }
                },
                {data: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Manage Suppliers</h5>
                    <a href="{{ route('user.supplier.create') }}" class="btn btn-sm btn-primary float-right"><i
                            class="fa fa-plus"></i>
                        Add Supplier</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>Company Name</th>
                                    <th>Account Name</th>
                                    <th>Email</th>
                                    <th width="75">Role</th>
                                    <th width="65">Status</th>
                                    <th width="95" align="center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
