@extends('layouts.app')

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('title', 'Manage Customers')

@section('breadcrumb')
<li class="breadcrumb-item active">Customers</li>
@endsection

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            responsive: true,
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Manage Customers</h5>
                    <a href="{{ route('user.customer.create') }}" class="btn btn-sm btn-primary float-right"><i
                            class="fa fa-plus"></i>
                        Add Customer</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th width="75">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
