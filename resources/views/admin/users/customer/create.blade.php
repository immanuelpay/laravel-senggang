@extends('layouts.app')

@section('title', 'Create Customer')

@section('breadcrumb')
<li class="breadcrumb-item">Customer</li>
<li class="breadcrumb-item active">Create</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}}}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
@endpush

@section('content')
<form method="POST" action="{{ route('user.customer.store') }}" enctype="multipart/form-data">
    @csrf

    <div class="container-fluid">
        <div class="product-adding">
            <div class="card">
                <div class="card-header">
                    <h5>Add Customer Info</h5>
                </div>
                <div class="card-body">
                    <div class="digital-add needs-validation">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Class</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Room</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Building</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Address 1</label>
                                    <textarea class="form-control" name="address1" id="address1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Address 2</label>
                                    <textarea class="form-control" name="address2" id="address2" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> City</label>
                                    <select class="form-control" name="city" id="city">
                                        <option>--Select City--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> State</label>
                                    <select class="form-control" name="state" id="state">
                                        <option>--Select State--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Postal Code</label>
                                    <select class="form-control" name="postalCode" id="postalCode">
                                        <option>--Select Postal Code--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Country</label>
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Country--</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Credit Card</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Credit Type ID</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Card Exp Month</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="stock" class="col-form-label"><span>*</span> Card Exp Year</label>
                                    <input class="form-control" name="stock" id="stock" type="text" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row product-adding">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Shipping</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address1" id="address1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">City</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select City--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Region</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Region--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Postal Code</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Postal Code--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Country</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Country--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Billing</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address1" id="address1" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">City</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select City--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Region</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Region--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Postal Code</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Postal Code--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Country</label>
                                <div class="col-8">
                                    <select class="form-control" name="country" id="country">
                                        <option>--Select Country--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row product-adding">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Identity Image</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">KTP</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">KK</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>Add Customer Account</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Username</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="email" class="col-4 col-form-label">Email</label>
                                <div class="col-8">
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password" class="col-4 col-form-label">Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password_confirmation" class="col-4 col-form-label">Confirm
                                    Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password_confirmation"
                                        name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Status</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="edo-ani">
                                        <input class="radio_animated" id="edo-ani" type="radio" name="status"
                                            value="ACTIVE">
                                        Active
                                    </label>
                                    <label class="d-block" for="edo-ani1">
                                        <input class="radio_animated" id="edo-ani1" type="radio" name="status"
                                            value="INACTIVE">
                                        Inactive
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">Avatar</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Customer Attribute</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="dob" class="col-4 col-form-label">DOB</label>
                                <div class="col-8">
                                    <input type="date" class="form-control" id="dob" name="dob">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Gender</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="edo-ani">
                                        <input class="radio_animated" id="edo-ani" type="radio" name="gender"
                                            value="Male">
                                        Male
                                    </label>
                                    <label class="d-block" for="edo-ani1">
                                        <input class="radio_animated" id="edo-ani1" type="radio" name="gender"
                                            value="Female">
                                        Female
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="address" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address" id="address" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="phone" class="col-4 col-form-label">Phone</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="facebook" class="col-4 col-form-label">Facebook</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="facebook" name="facebook">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="instagram" class="col-4 col-form-label">Instagram</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="instagram" name="instagram">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="twitter" class="col-4 col-form-label">Twitter</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="twitter" name="twitter">
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <a href="{{ route('user.customer.index') }}" class="btn btn-light"><i
                                        class="fa fa-arrow-left"></i>
                                    Back</a>
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>
                                    Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Container-fluid Ends-->
@endsection
