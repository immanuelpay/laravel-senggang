@extends('layouts.app')

@section('title', 'Edit Administrator')

@section('breadcrumb')
<li class="breadcrumb-item">Administrator</li>
<li class="breadcrumb-item active">Edit</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}}}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
@endpush

@section('content')
<form method="POST" action="{{ route('user.administrator.update', ['id' => $administrator->id]) }}"
    enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="container-fluid">
        <div class="row product-adding">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Administrator : <b>{{ $administrator->name }}</b></h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Name</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ $administrator->name }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Username</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="username" name="username"
                                        value="{{ $administrator->username }}">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="email" class="col-4 col-form-label">Email</label>
                                <div class="col-8">
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ $administrator->email }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Status</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="edo-ani">
                                        <input class="radio_animated" id="edo-ani" type="radio" name="status"
                                            value="ACTIVE" {{ $administrator->status == 'ACTIVE' ? 'checked' : '' }}>
                                        Active
                                    </label>
                                    <label class="d-block" for="edo-ani1">
                                        <input class="radio_animated" id="edo-ani1" type="radio" name="status"
                                            value="INACTIVE"
                                            {{ $administrator->status == 'INACTIVE' ? 'checked' : '' }}>
                                        Inactive
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="avatar" class="col-4 col-form-label">Avatar</label>
                                <div class="col-8">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico" class="form-control"
                                        name="avatar" id="avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>Change Password</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="password" class="col-4 col-form-label">New Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="password_confirmation" class="col-4 col-form-label">Confirm Password</label>
                                <div class="col-8">
                                    <input type="password" class="form-control" id="password_confirmation"
                                        name="password_confirmation">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Attribute</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group row ">
                                <label for="dob" class="col-4 col-form-label">DOB</label>
                                <div class="col-8">
                                    <input type="date" class="form-control" id="dob" name="dob" value="{{ $dob }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Gender</label>
                                <div class="col-8 m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="edo-ani">
                                        <input class="radio_animated" id="edo-ani" type="radio" name="gender"
                                            value="Male" {{ $attribute->gender == 'Male' ? 'checked' : '' }}>
                                        Male
                                    </label>
                                    <label class="d-block" for="edo-ani1">
                                        <input class="radio_animated" id="edo-ani1" type="radio" name="gender"
                                            value="Female" {{ $attribute->gender == 'Female' ? 'checked' : '' }}>
                                        Female
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="address" class="col-4 col-form-label">Address</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address" id="address"
                                        rows="5">{{ $attribute->address }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="phone" class="col-4 col-form-label">Phone</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="phone" name="phone"
                                        value="{{ $attribute->phone }}">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="facebook" class="col-4 col-form-label">Facebook</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="facebook" name="facebook"
                                        value="{{ $attribute->facebook }}">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="instagram" class="col-4 col-form-label">Instagram</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="instagram" name="instagram"
                                        value="{{ $attribute->instagram }}">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="twitter" class="col-4 col-form-label">Twitter</label>
                                <div class="col-8">
                                    <input type="text" class="form-control" id="twitter" name="twitter"
                                        value="{{ $attribute->twitter }}">
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <a href="{{ route('user.administrator.index') }}" class="btn btn-light"><i
                                        class="fa fa-arrow-left"></i>
                                    Back</a>
                                <button type="submit" class="btn btn-primary float-right"><i class="fa fa-check"></i>
                                    Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
