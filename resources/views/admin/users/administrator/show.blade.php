@extends('layouts.app')

@section('title', 'Show Administrator')

@section('breadcrumb')
<li class="breadcrumb-item">Administrator</li>
<li class="breadcrumb-item active">Show</li>
@endsection

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="profile-details text-center">
                        <img src="{{ asset('images/user/' . Auth::user()->avatar) }}" alt=""
                            class="img-fluid img-90 rounded-circle blur-up lazyloaded">
                        <h5 class="f-w-600 mb-0">{{ auth()->user()->name }}</h5>
                        <span>{{ auth()->user()->email }}</span>
                        <div class="social">
                            <div class="form-group btn-showcase">
                                <a href="https://facebook.com/{{ $attribute->facebook }}"
                                    class="btn social-btn btn-fb d-inline-block"> <i class="fa fa-facebook"></i></a>
                                <a href="https://instagram.com/{{ $attribute->instagram }}"
                                    class="btn social-btn btn-google d-inline-block"><i class="fa fa-instagram"></i></a>
                                <a href="https://twitter.com/{{ $attribute->twitter }}"
                                    class="btn social-btn btn-twitter d-inline-block mr-0"><i
                                        class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card tab2-card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="top-profile-tab" data-toggle="tab"
                                href="#top-profile" role="tab" aria-controls="top-profile" aria-selected="true"><i
                                    data-feather="user" class="mr-2"></i>Show Administrator</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="top-tabContent">
                        <div class="tab-pane fade show active" id="top-profile" role="tabpanel"
                            aria-labelledby="top-profile-tab">
                            <h5 class="f-w-600 mb-4">Profile</h5>
                            <div class="form-group row">
                                <label class="col-3">Name</label>
                                <label class="col-9">{{ $administrator->name }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Username</label>
                                <label class="col-9">{{ $administrator->username }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Email</label>
                                <label class="col-9">{{ $administrator->email }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Gender</label>
                                <label class="col-9">{{ $attribute->gender }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">DOB</label>
                                <label class="col-9">{{ $dob }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Phone</label>
                                <label class="col-9">{{ $attribute->phone }}</label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Address</label>
                                <label class="col-9">{{ $attribute->address }}</label>
                            </div>
                            <a href="{{ route('user.administrator.index') }}" class="btn btn-light"><i
                                    class="fa fa-arrow-left"></i>
                                Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
