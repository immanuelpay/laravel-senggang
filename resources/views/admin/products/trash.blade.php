@extends('layouts.app')

@section('title', 'Trash Products')

@section('breadcrumb')
<li class="breadcrumb-item">Products</li>
<li class="breadcrumb-item active">Trash</li>
@endsection

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('products.trash') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'sku'},
                {data: 'title'},
                {data: 'categoryID'},
                {data: 'price'},
                {data: 'stock'},
                {
                    data: 'status',
                    name: 'status',
                    render: function (data, type, full, meta)
                    {
                        if (data == "DISABLE") {
                        return '<span class="badge badge-danger">'+ data +'</span>';
                        } else {
                        return '<span class="badge badge-success">'+ data +'</span>';
                        }
                    }
                },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Trash Products</h5>
                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-default float-right"><i
                            class="fa fa-arrow-left"></i>
                        Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Stock</th>
                                    <th>Status</th>
                                    <th width="58" align="center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
