@extends('layouts.app')

@section('title', 'Show Products')

@section('breadcrumb')
<li class="breadcrumb-item">Product</li>
<li class="breadcrumb-item active">Show</li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Show Products : <b>{{ $product->title }}</b></h5>
                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-default float-right"><i
                            class="fa fa-arrow-left"></i>
                        Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-6">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-inner">
                                        @php
                                        $i = 0;
                                        @endphp
                                        @foreach (json_decode($product->images) as $image)
                                        @php
                                        $i++;
                                        @endphp
                                        @if ($i == 1)
                                        <div class="carousel-item active">
                                            <img src="{{ asset($path_medium . '/' . $image) }}" class="d-block w-100"
                                                alt="...">
                                        </div>
                                        @else
                                        <div class="carousel-item">
                                            <img src="{{ asset($path_medium . '/' . $image) }}" class="d-block w-100"
                                                alt="...">
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <a href="{{ route('products.download.image', ['slug' => $product->slug]) }}"
                                class="btn btn-block btn-info mt-3"><i class="fa fa-download"></i> Download</a>
                        </div>

                        <div class="col-6">
                            <p>
                                <h5>{{ $product->title }} <span class="badge badge-warning">{{ $product->profit }}%
                                        profit.</span></h5>
                            </p>
                            <h5><strike>{{ 'IDR ' . number_format($product->price, 2) }}</strike></h5>
                            <div class="row">
                                <div class="col-6">
                                    <h4>{{ 'IDR ' . number_format($product->price - ($product->price * $product->profit / 100), 2) }}
                                    </h4>
                                </div>
                                <div class="col-6">
                                    <h5 class="float-right">Stock : {{ $product->stock }} packs.</h5>
                                </div>
                            </div>
                            <hr>
                            <p>
                                <h6>Product Details</h6>
                                <small>{{ $product->short_summary }}</small>
                            </p>
                            <hr>
                            <div class="row">
                                <div class="col-6">
                                    <h6> Status :
                                        @if ($product->status == "DISABLE")
                                        <span class="badge badge-danger">{{ $product->status }}</span>
                                        @else
                                        <span class="badge badge-success">{{ $product->status }}</span>
                                        @endif
                                    </h6>
                                </div>
                                <div class="col-6">
                                    <small>Created by : {{ $user->name }}</small><br>
                                    <small>Email : {{ $user->email }}</small><br>
                                    <small>Username : {{ $user->username }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card tab2-card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="top-description-tab" data-toggle="tab"
                                href="#top-description" role="tab" aria-controls="top-description"
                                aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" id="detail-top-tab" data-toggle="tab"
                                href="#top-detail" role="tab" aria-controls="top-detail"
                                aria-selected="false">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="top-tabContent">
                        <div class="tab-pane fade show active" id="top-description" role="tabpanel"
                            aria-labelledby="top-description-tab">
                            {!! $product->description !!}
                        </div>
                        <div class="tab-pane fade" id="top-detail" role="tabpanel" aria-labelledby="detail-top-tab">
                            {!! $product->detail !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
