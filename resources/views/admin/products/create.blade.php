@extends('layouts.app')

@section('title', 'Create Product')

@section('breadcrumb')
<li class="breadcrumb-item">Product</li>
<li class="breadcrumb-item active">Create</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/styles.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description', {
        on: {
            contentDom: function( evt ) {
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();
                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    });

    CKEDITOR.replace( 'detail', {
        on: {
            contentDom: function( evt ) {
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();
                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    });

    $(document).ready(function () {
        $('.btn-image-add').click(function() {
            var lsthtml = $('.clone').html();
            $('.increment').after(lsthtml);
        });

        $('body').on('click', '#btn-image-remove', function() {
            $(this).parents('.lst').remove();
        });
    });
</script>
@endpush

@section('content')
<!-- Container-fluid starts-->

<form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
    @csrf

    <div class="container-fluid">
        <div class="row product-adding">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Product</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group">
                                <label for="title" class="col-form-label pt-0"><span>*</span> Title</label>
                                <input class="form-control" name="title" id="title" type="text" required="">
                            </div>
                            <div class="form-group">
                                <label for="sku" class="col-form-label pt-0"><span>*</span> SKU</label>
                                <input class="form-control" name="sku" id="sku" type="text" required="">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label"><span>*</span> Categories</label>
                                <select class="custom-select" name="category" required="">
                                    <option>--Select--</option>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"> {{ $category->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Sort Summary</label>
                                <textarea class="form-control" name="short_summary" rows="7" cols="12"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="stock" class="col-form-label"><span>*</span> Stock</label>
                                <input class="form-control" name="stock" id="stock" type="text" required="">
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="price" class="col-form-label"><span>*</span> Product
                                        Price</label>
                                    <input class="form-control" name="price" id="price" type="text" required="">
                                </div>
                                <div class="form-group col-6">
                                    <label for="profit" class="col-form-label"><span>*</span> Profit (%)</label>
                                    <input class="form-control" name="profit" id="profit" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label"><span>*</span> Status</label>
                                <div class="m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="edo-ani">
                                        <input class="radio_animated" id="edo-ani" type="radio" name="status"
                                            value="ENABLE">
                                        Enable
                                    </label>
                                    <label class="d-block" for="edo-ani1">
                                        <input class="radio_animated" id="edo-ani1" type="radio" name="status"
                                            value="DISABLE">
                                        Disable
                                    </label>
                                </div>
                            </div>
                            <label class="col-form-label pt-0"> Product Upload</label>
                            <div class="input-group increment">
                                <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                    class="myfrm form-control mr-3 pr-0" name="images[]" id="images">
                                <button class="btn btn-success btn-image-add" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <div class="clone visible">
                                <div class="image-file control-group lst input-group mt-3">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                        class="form-control mr-3 pr-0" name="images[]" id="images-remove">
                                    <button class="btn btn-danger btn-image-remove" type="button" id="btn-image-remove">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted"><i>Kosongkan jika tidak ingin mengupload gambar.</i></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Add Description</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group mb-0">
                                <div class="description-sm">
                                    <textarea id="description" name="description" cols="10" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Add Detail</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group">
                                <textarea id="detail" name="detail" rows="4" cols="12"></textarea>
                            </div>
                            <div class="form-group mb-0">
                                <div class="product-buttons">
                                    <a href="{{ route('products.index') }}" class="btn btn-light"><i
                                            class="fa fa-arrow-left"></i>
                                        Back</a>
                                    <button type="submit" class="btn btn-primary float-right"><i
                                            class="fa fa-check"></i>
                                        Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<!-- Container-fluid Ends-->

@endsection
