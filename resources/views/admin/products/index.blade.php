@extends('layouts.app')

@section('title', 'Manage Products')

@section('breadcrumb')
<li class="breadcrumb-item active">Products</li>
@endsection

@push('extraCSS')
<link href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('datatables/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('extraJS')
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('datatables/js/buttons.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('products.index') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'sku'},
                {data: 'title'},
                {data: 'categoryID'},
                {data: 'price'},
                {data: 'stock'},
                {
                    data: 'status',
                    render: function (data, type, full, meta)
                    {
                        if (data == "DISABLE") {
                        return '<span class="badge badge-danger">'+ data +'</span>';
                        } else {
                        return '<span class="badge badge-success">'+ data +'</span>';
                        }
                    }
                },
                {data: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Manage Products</h5>
                    <a href="{{ route('products.create') }}" class="btn btn-sm btn-primary float-right"><i
                            class="fa fa-plus"></i> Add
                        Product</a>
                    <a href="{{ route('products.trash') }}" class="btn btn-sm btn-danger float-right mr-3"><i
                            class="fa fa-recycle"></i>
                        Trash Product</a>
                    <button type="button" onclick="$('#datatable').DataTable().ajax.reload();"
                        class="btn btn-sm btn-success float-right mr-3"><i class="fa fa-refresh"></i>
                        Refresh</button>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="datatable">
                            <thead>
                                <tr>
                                    <th width="35">No</th>
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Stock</th>
                                    <th>Status</th>
                                    <th width="95" align="center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
