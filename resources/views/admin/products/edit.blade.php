@extends('layouts.app')

@section('title', 'Edit Product')

@section('breadcrumb')
<li class="breadcrumb-item">Product</li>
<li class="breadcrumb-item active">Edit</li>
@endsection

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dropzone.css') }}">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/dropzone/dropzone-script.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/styles.js') }}"></script>
<script src="{{ asset('assets/js/editor/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'description', {
        on: {
            contentDom: function( evt ) {
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();
                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    });

    CKEDITOR.replace( 'detail', {
        on: {
            contentDom: function( evt ) {
                evt.editor.editable().on( 'contextmenu', function( contextEvent ) {
                    var path = evt.editor.elementPath();
                    if ( !path.contains( 'table' ) ) {
                        contextEvent.cancel();
                    }
                }, null, null, 5 );
            }
        }
    });

    $(document).ready(function () {
        $('.btn-image-add').click(function() {
            var lsthtml = $('.clone').html();
            $('.increment').after(lsthtml);
        });

        $('body').on('click', '#btn-image-remove', function() {
            $(this).parents('.lst').remove();
        });
    });
</script>
@endpush

@section('content')
<form method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="container-fluid">
        <div class="row product-adding">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Product : <b>{{ $product->title }}</b></h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group">
                                <label for="title" class="col-form-label pt-0"><span>*</span> Title</label>
                                <input class="form-control" name="title" id="title" type="text"
                                    value="{{ $product->title }}">
                            </div>
                            <div class="form-group">
                                <label for="sku" class="col-form-label pt-0"><span>*</span> SKU</label>
                                <input class="form-control" name="sku" id="sku" type="text" value="{{ $product->sku }}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label"><span>*</span> Categories</label>
                                <select class="custom-select" name="category">
                                    <option>--Select--</option>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ $category->id == $product->categoryID ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Sort Summary</label>
                                <textarea name="short_summary" rows="7" class="form-control"
                                    cols="12">{!! $product->short_summary !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="stock" class="col-form-label"><span>*</span> Stock</label>
                                <input class="form-control" name="stock" id="stock" type="text"
                                    value="{{ $product->stock }}">
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="price" class="col-form-label"><span>*</span> Product
                                        Price</label>
                                    <input class="form-control" name="price" id="price" type="money"
                                        value="{{ $product->price }}">
                                </div>
                                <div class="form-group col-6">
                                    <label for="profit" class="col-form-label"><span>*</span> Profit (%)</label>
                                    <input class="form-control" name="profit" id="profit" type="text"
                                        value="{{ $product->profit }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label"><span>*</span> Status</label>
                                <div class="m-checkbox-inline mb-0 custom-radio-ml d-flex radio-animated">
                                    <label class="d-block" for="ENABLE">
                                        <input class="radio_animated" id="ENABLE" type="radio" name="status"
                                            value="ENABLE" {{ $product->status == 'ENABLE' ? 'checked' : '' }}>
                                        Enable
                                    </label>
                                    <label class="d-block" for="DISABLE">
                                        <input class="radio_animated" id="DISABLE" type="radio" name="status"
                                            value="DISABLE" {{ $product->status == 'DISABLE' ? 'checked' : '' }}>
                                        Disable
                                    </label>
                                </div>
                            </div>
                            <label class="col-form-label pt-0"> Product Upload</label>
                            <div class="input-group increment">
                                <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                    class="myfrm form-control mr-3 pr-0" name="images[]" id="images">
                                <button class="btn btn-success btn-image-add" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <div class="clone visible">
                                <div class="image-file control-group lst input-group mt-3">
                                    <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                        class="form-control mr-3 pr-0" name="images[]" id="images-remove">
                                    <button class="btn btn-danger btn-image-remove" type="button" id="btn-image-remove">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <small class="text-muted"><i>Kosongkan jika tidak ingin mengupload gambar.</i></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Description</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group mb-0">
                                <div class="description-sm">
                                    <textarea id="description" name="description" cols="10"
                                        rows="4">{!! $product->description !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Detail</h5>
                    </div>
                    <div class="card-body">
                        <div class="digital-add needs-validation">
                            <div class="form-group">
                                <textarea id="detail" name="detail" rows="4"
                                    cols="12">{!! $product->detail !!}</textarea>
                            </div>
                            <div class="form-group mb-0">
                                <div class="product-buttons">
                                    <a href="{{ route('products.index') }}" class="btn btn-light"><i
                                            class="fa fa-arrow-left"></i>
                                        Back</a>
                                    <button type="submit" class="btn btn-primary float-right"><i
                                            class="fa fa-check"></i>
                                        Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection
