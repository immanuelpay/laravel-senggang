@extends('layouts.app')

@section('title', 'Profile Setting')

@section('breadcrumb')
<li class="breadcrumb-item">Profile</li>
<li class="breadcrumb-item active">Setting</li>
@endsection

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <div class="profile-details text-center">
                        <img src="{{ asset('images/user/' . Auth::user()->avatar) }}" alt=""
                            class="img-fluid img-90 rounded-circle blur-up lazyloaded">
                        <h5 class="f-w-600 mb-0">{{ auth()->user()->name }}</h5>
                        <span>{{ auth()->user()->email }}</span>
                        <div class="social">
                            <div class="form-group btn-showcase">
                                <a href="https://facebook.com/{{ $attribute->facebook }}"
                                    class="btn social-btn btn-fb d-inline-block"> <i class="fa fa-facebook"></i></a>
                                <a href="https://instagram.com/{{ $attribute->instagram }}"
                                    class="btn social-btn btn-google d-inline-block"><i class="fa fa-instagram"></i></a>
                                <a href="https://twitter.com/{{ $attribute->twitter }}"
                                    class="btn social-btn btn-twitter d-inline-block mr-0"><i
                                        class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card tab2-card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="top-profile-tab" data-toggle="tab"
                                href="#top-profile" role="tab" aria-controls="top-profile" aria-selected="true"><i
                                    data-feather="user" class="mr-2"></i>Edit Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" id="contact-top-tab" data-toggle="tab"
                                href="#top-contact" role="tab" aria-controls="top-contact" aria-selected="false"><i
                                    data-feather="lock" class="mr-2"></i>Change Password</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="top-tabContent">
                        <div class="tab-pane fade show active" id="top-profile" role="tabpanel"
                            aria-labelledby="top-profile-tab">
                            <h5 class="f-w-600">Edit Profile</h5>

                            <form action="{{ route('profile.setting') }}" method="POST" class="mt-3"
                                enctype="multipart/form-data">
                                @csrf
                                @method('POST')

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="name" name="name"
                                            value="{{ $user->name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="username" class="col-sm-4 col-form-label">Username</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="username" name="username"
                                            value="{{ $user->username }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dob" class="col-sm-4 col-form-label">DOB</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" id="dob" name="dob"
                                            value="@if (isset($attribute)){{ $dob }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dob" class="col-sm-4 col-form-label">Gender</label>
                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="male"
                                                value="Male" @if (isset($attribute))
                                                {{ $attribute->gender == 'Male' ? 'checked' : '' }} @endif>
                                            <label class="form-check-label" for="male">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="female"
                                                value="Female" @if (isset($attribute))
                                                {{ $attribute->gender == 'Female' ? 'checked' : '' }} @endif>
                                            <label class="form-check-label" for="female">Female</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" id="email" name="email"
                                            value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="facebook" class="col-sm-4 col-form-label">Facebook</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="facebook" name="facebook"
                                            value="@if (isset($attribute)){{ $attribute->facebook }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="instagram" class="col-sm-4 col-form-label">Instagram</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="instagram" name="instagram"
                                            value="@if (isset($attribute)){{ $attribute->instagram }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="twitter" class="col-sm-4 col-form-label">Twitter</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="twitter" name="twitter"
                                            value="@if (isset($attribute)){{ $attribute->twitter }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="phone" name="phone"
                                            value="@if (isset($attribute)){{ $attribute->phone }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="address" class="col-sm-4 col-form-label">Address</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="address" id="address" rows="5">
                                            @if (isset($attribute)){{ $attribute->address }}@endif
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label for="avatar" class="col-4 col-form-label">Avatar</label>
                                    <div class="col-8">
                                        <input type="file" accept=".jpeg, .png, .jpg, .gif, .svg, .ico"
                                            class="form-control" name="avatar" id="avatar">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                        Save</button>
                                </div>
                            </form>

                        </div>
                        <div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
                            <div class="account-setting deactivate-account">
                                <h5 class="f-w-600">Change Password</h5>
                                <form action="{{ route('change-password') }}" method="POST">
                                    @csrf
                                    @method('POST')

                                    <div class="form-group row">
                                        <label for="old_password" class="col-sm-4 col-form-label">Current
                                            Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" id="old_password" name="old_password"
                                                class="form-control {{$errors->first('old_password') ? "is-invalid" : ""}}">
                                            <div class="invalid-feedback">
                                                {{$errors->first('old_password')}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="new_password" class="col-sm-4 col-form-label">New Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" id="new_password" name="new_password"
                                                class="form-control {{$errors->first('new_password') ? "is-invalid" : ""}}">
                                            <div class="invalid-feedback">
                                                {{$errors->first('new_password')}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="confirm_password" class="col-sm-4 col-form-label">Confirm New
                                            Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" id="confirm_password" name="confirm_password"
                                                class="form-control {{$errors->first('confirm_password') ? "is-invalid" : ""}}">
                                            <div class="invalid-feedback">
                                                {{$errors->first('confirm_password')}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>
                                            Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
