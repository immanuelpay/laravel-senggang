<ul class="sidebar-menu">
    <li><a class="{{ (request()->is('home')) ? 'active' : '' }} sidebar-header" href="{{ route('home') }}"><i
                data-feather="home"></i><span>Dashboard</span></a>
    </li>
    <li><a class="{{ (request()->is('categories*')) ? 'active' : '' }} sidebar-header"
            href="{{ route('categories.index') }}"><i data-feather="clipboard"></i><span>Categories</span></a></li>
    <li><a class="{{ (request()->is('products*')) ? 'active' : '' }} sidebar-header"
            href="{{ route('products.index') }}"><i data-feather="box"></i><span>Products</span></a></li>
    <li><a class="sidebar-header" href=""><i data-feather="dollar-sign"></i><span>Sales</span><i
                class="fa fa-angle-right pull-right"></i></a>
        <ul class="sidebar-submenu">
            <li><a href="#"><i class="fa fa-circle"></i>Orders</a></li>
            <li><a href="#"><i class="fa fa-circle"></i>Transactions</a></li>
        </ul>
    </li>
    <li class="{{ (request()->is('users*')) ? 'active' : '' }}"><a
            class="{{ (request()->is('users*')) ? 'active' : '' }} sidebar-header" href=""><i
                data-feather="user-plus"></i><span>Users</span><i class="fa fa-angle-right pull-right"></i></a>
        <ul class="sidebar-submenu">
            <li><a class="{{ (request()->is('users/administrator*')) ? 'active' : '' }}"
                    href="{{ route('user.administrator.index') }}"><i class="fa fa-circle"></i>Administrators</a></li>
            <li><a class="{{ (request()->is('users/supplier*')) ? 'active' : '' }}"
                    href="{{ route('user.supplier.index') }}"><i class="fa fa-circle"></i>Suppliers</a></li>
            <li><a class="{{ (request()->is('users/customer*')) ? 'active' : '' }}"
                    href="{{ route('user.customer.index') }}"><i class="fa fa-circle"></i>Customers</a></li>
        </ul>
    </li>
    <li><a class="sidebar-header" href="#"><i data-feather="bar-chart"></i><span>Reports</span></a></li>
    <li class="{{ (request()->is('profile-setting')) ? 'active' : '' }}"><a
            class="{{ (request()->is('profile-setting')) ? 'active' : '' }} sidebar-header" href=""><i
                data-feather="settings"></i><span>Settings</span><i class="fa fa-angle-right pull-right"></i></a>
        <ul class="sidebar-submenu">
            <li><a class="{{ (request()->is('profile-setting')) ? 'active' : '' }}"
                    href="{{ route('profile.setting') }}"><i class="fa fa-circle"></i>Profile</a></li>
        </ul>
    </li>
    <li><a class="sidebar-header" href="#"><i data-feather="archive"></i><span>Invoice</span></a>
    </li>
    <li><a class="sidebar-header" href="#" data-toggle="modal" data-target="#modalLogout"><i
                data-feather="log-out"></i><span>Logout</span></a>
    </li>
</ul>
