<!-- Modal Show-->
<div class="modal fade" id="modalShow" tabindex="-1" role="dialog" aria-labelledby="modalShowLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalShowLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="bodyShow">
                <img class="categoryImage img-thumbnail mx-auto d-block" align="center"
                    src="{{ url('/images/user/avatar.png') }}" alt="category" width="150px">
                <div class="form-gruop row mt-3">
                    <label class="offset-2 col-3"> Name</label>
                    <label class="categoryName col-7"></label>
                </div>
                <div class="form-gruop row">
                    <label class="offset-2 col-3"> Slug</label>
                    <label class="categorySlug col-7"></label>
                </div>
                <div class="form-gruop row">
                    <label class="offset-2 col-3"> Parent</label>
                    <label class="categoryParent col-7"></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-remove"></i>
                    Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete-->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalDeleteLabel">Delete Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="bodyDelete">
                <text id="deleteTitle"></text> <b id="deleteName"></b> <text id="deleteTitle1"></text>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-remove"></i>
                    Close</button>
                <a href="" class="btn btn-danger" id="linkDelete"><i class="fa fa-trash"></i> Yes</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal Logout-->
<div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLogoutLabel">Logout User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure loged out and exit system?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-remove"></i>
                    Close</button>
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-danger"><i class="fa fa-sign-out"></i> Logout</button>
                </form>
            </div>
        </div>
    </div>
</div>
