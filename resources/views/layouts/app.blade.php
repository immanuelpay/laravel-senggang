<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
            content="Bigdeal admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords"
            content="admin template, Bigdeal admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="pixelstrap">
        <link rel="icon" href="{{ asset('assets/images/favicon/favicon.ico') }}" type="image/x-icon">
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon/favicon.ico') }}" type="image/x-icon">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
        @include('layouts._style')
        @stack('extraCSS')
    </head>

    <body>

        <!-- page-wrapper Start-->
        <div class="page-wrapper">

            <!-- Page Header Start-->
            <div class="page-main-header">
                <div class="main-header-left">
                    <div class="logo-wrapper text-center"><a href="index.html"><img class="blur-up lazyloaded"
                                src="{{ asset('assets/images/layout-2/logo/logo.png') }}" alt=""></a></div>
                </div>
                <div class="main-header-right row">
                    <div class="mobile-sidebar">
                        <div class="media-body text-right switch-sm">
                            <label class="switch">
                                <input id="sidebar-toggle" type="checkbox" checked="checked"><span
                                    class="switch-state"></span>
                            </label>
                        </div>
                    </div>
                    <div class="nav-right col">
                        <ul class="nav-menus">
                            <li>
                                <form class="form-inline search-form">
                                    <div class="form-group">
                                        <input class="form-control-plaintext" type="search" placeholder="Search.."><span
                                            class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                                    </div>
                                </form>
                            </li>
                            <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i
                                        data-feather="maximize"></i></a></li>
                            <li class="onhover-dropdown">
                                <div class="media align-items-center"><img
                                        class="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded"
                                        src="{{ asset('images/user/' . Auth::user()->avatar) }}" alt="header-user">
                                    <div class="dotted-animation"><span class="animate-circle"></span><span
                                            class="main-circle"></span></div>
                                </div>
                                <ul class="profile-dropdown onhover-show-div p-20 profile-dropdown-hover">
                                    <li><a href="{{ route('profile') }}">Profile<span class="pull-right"><i
                                                    data-feather="user"></i></span></a></li>
                                    <li><a href="{{ route('profile.setting') }}">Settings<span class="pull-right"><i
                                                    data-feather="settings"></i></span></a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#modalLogout">Logout<span
                                                class="pull-right"><i data-feather="log-out"></i></span></a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
                    </div>
                </div>
            </div>
            <!-- Page Header Ends -->

            <!-- Page Body Start-->
            <div class="page-body-wrapper">

                <!-- Page Sidebar Start-->
                <div class="page-sidebar">
                    <div class="sidebar custom-scrollbar">
                        <div class="sidebar-user text-center">
                            <div><img class="img-60 rounded-circle lazyloaded blur-up"
                                    src="{{ asset('images/user/' . Auth::user()->avatar) }}" alt="#">
                            </div>
                            <h6 class="mt-3 f-14">{{ Auth::user()->name }}</h6>
                            <p>{{ Auth::user()->roles }}</p>
                        </div>

                        @include('layouts._menu')

                    </div>
                </div>
                <!-- Page Sidebar Ends-->

                <div class="page-body">

                    <!-- Container-fluid starts-->
                    <div class="container-fluid">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="page-header-left">
                                        <h3>@yield('title')
                                            <small>Senggang | AdminPanel</small>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <ol class="breadcrumb pull-right">
                                        <li class="breadcrumb-item"><a href="{{ route('home') }}"><i
                                                    data-feather="home"></i></a>
                                        </li>
                                        @yield('breadcrumb')
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Container-fluid Ends-->

                    @yield('content')

                </div>

                <!-- footer start-->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 footer-copyright">
                                <p class="mb-0">Copyright {{ date('Y') }} © Senggang.com All rights reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right mb-0">Hand crafted & made with<i class="fa fa-heart"></i></p>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- footer end-->

            </div>
        </div>

        @include('layouts._modals')
        @include('layouts._script')
        @include('sweetalert::alert')
        @stack('extraJS')
    </body>

</html>
