@extends('layouts.appCustomer')

@section('title', 'Dashboard')

@push('extraCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/icofont.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/prism.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/chartist.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vector-map.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/themify.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/slick-theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/color3.css') }}" media="screen" id="color">
@endpush

@push('extraJS')
<script src="{{ asset('assets/js/chart/chartist/chartist.js') }}"></script>
<script src="{{ asset('assets/js/prism/prism.min.js') }}"></script>
<script src="{{ asset('assets/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{ asset('assets/js/custom-card/custom-card.js') }}"></script>
<script src="{{ asset('assets/js/counter/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/js/counter/counter-custom.js') }}"></script>
<script src="{{ asset('assets/js/vector-map/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ asset('assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('assets/js/chart/apex-chart/apex-chart.js') }}"></script>
<script src="{{ asset('assets/js/chart/apex-chart/stock-prices.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/excanvas.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/jquery.flot.time.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/jquery.flot.categories.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('assets/js/chart/flot-chart/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('assets/js/dashboard/default.js') }}"></script>
<script src="{{ asset('assets/js/equal-height.js') }}"></script>
<script src="{{ asset('assets/js/slick.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/js/slider-animat-three.js') }}"></script>
<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('assets/js/modal.js') }}"></script>
@endpush

@section('content')
<!--top brand panel start-->
<section class="brand-panel">
    <div class="brand-panel-box">
        <div class="brand-panel-contain">
            <ul>
                <li><a href="#">top brand</a></li>
                <li><a>:</a></li>
                <li><a href="#">aerie</a></li>
                <li><a href="#">baci lingrie</a></li>
                <li><a href="#">gerbe</a></li>
                <li><a href="#">jolidon</a></li>
                <li><a href="#">Wonderbra </a></li>
                <li><a href="#">Ultimo</a></li>
                <li><a href="#"> Vassarette </a></li>
                <li><a href="#">Oysho</a></li>
            </ul>
        </div>
    </div>

</section>
<!--top brand panel end-->

<!--title start-->
<div class="title1 section-mb-space">
    <h4>New Products</h4>
</div>
<!--title end-->

<!--product start-->
<section class="product section-pb-space ">
    <div class="custom-container">
        <div class="row">
            <div class="col pr-0">
                <div class="product-slide-6 no-arrow">

                    @foreach ($products as $product)
                    @php
                    $user = App\User::where('id', $product->created_by)->first();
                    $path_medium = 'images/product/medium/' . $user->id . '_' . $user->username . '/' . '._' .
                    $product->code;
                    $image = json_decode($product->images);
                    @endphp
                    <div>
                        <div class="product-box">
                            <div class="product-imgbox">
                                <div class="product-front">
                                    <img src="{{ asset($path_medium . '/' .$image[0]) }}" class="img-fluid  "
                                        alt="product">
                                </div>
                                <div class="product-icon icon-center">
                                    <div>
                                        <button onclick="openCart()" type="button">
                                            <i class="ti-bag"></i>
                                        </button>
                                        <a href="javascript:void(0)" title="Add to Wishlist">
                                            <i class="ti-heart" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View">
                                            <i class="ti-search" aria-hidden="true"></i>
                                        </a>
                                        <a href="compare.html" title="Compare">
                                            <i class="fa fa-exchange" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="new-label2 badge badge-info text-white">
                                    <div>{{ $product->profit.'% profit.' }}</div>
                                </div>
                            </div>
                            <div class="product-detail detail-inline detail-inverse">
                                <div class="detail-title">
                                    <div class="detail-left">
                                        <div class="rating-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <a href="">
                                            <h6 class="price-title">
                                                {{ $product->title }}
                                            </h6>
                                        </a>
                                    </div>
                                    <div class="detail-right">
                                        <div class="check-price">
                                            {{ 'IDR ' . number_format($product->price, 2) }}
                                        </div>
                                        <div class="price">
                                            <div class="price">
                                                {{ 'IDR ' . number_format($product->price - ($product->price * $product->profit / 100), 2) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!--product end-->

<!--title start-->
<div class="title1 section-mb-space">
    <h4>categories</h4>
</div>
<!--title end-->

<!--product start-->
<section class="product section-pb-space ">
    <div class="custom-container">
        <div class="row">
            <div class="col pr-0">
                <div class="product-slide-6 no-arrow">

                    @foreach ($categories as $category)
                    <div>
                        <div class="product-box">
                            <div class="product-imgbox">
                                <div class="product-front">
                                    <img src="{{ asset('images/category/medium/'. $category->image) }}"
                                        class="img-fluid  " alt="product">
                                </div>
                                <div class="product-icon icon-center">

                                    <div>
                                        <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View">
                                            <i class="ti-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-detail detail-inline detail-inverse">
                                <div class="detail-title">
                                    <div class="text-center">
                                        <a href="">
                                            <h5 class="price-title">
                                                {{ $category->name }}
                                            </h5>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
@endsection