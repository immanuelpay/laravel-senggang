<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Senggang.com</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link rel="icon" href="{{ asset('assets/images/favicon/favicon.ico') }}" type="image/x-icon">
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
            rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <!--==========================
  Header
  ============================-->
        <header id="header" class="fixed-top">
            <div class="container">

                <div class="logo float-left">
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
                    <a href="#beranda" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
                </div>

                <nav class="main-nav float-right d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="#beranda">Beranda</a></li>
                        <li><a href="#tentang">Tentang Kami</a></li>
                        <li><a href="#contact">Kontak Kami</a></li>
                        <li><a href="{{ route('front.index') }}">Gabung</a></li>
                        <li><a href="{{ route('login') }}">Masuk</a></li>
                    </ul>
                </nav>
                <!-- .main-nav -->

            </div>
        </header>
        <!-- #header -->

        <!--==========================
    beranda Section
  ============================-->
        <section id="beranda" class="clearfix">
            <div class="container">

                <div class="beranda-img">
                    <img src="img/intro-img.svg" alt="" class="img-fluid">
                </div>

                <div class="beranda-info">
                    <h2><br>Memberdayakan dan Mengembangkan<br>Pelaku Usaha dan Investor!</h2>
                    <div>
                        <a href="#tentang" class="btn-get-started scrollto">Tentang Kami</a>
                        <a href="#contact" class="btn-services scrollto">Kontak Kami</a>
                    </div>
                </div>

            </div>
        </section>
        <!-- #beranda -->

        <main id="main">

            <!--==========================
      tentang Us Section
    ============================-->
            <section id="tentang">
                <div class="container">

                    <header class="section-header">
                        <h3>Tentang Kami</h3>
                        <p>
                            <b>Senggang.com </b> merupakan platform yang bergerak di akses permodalan yang menyasar pada
                            kalangan usaha mikro, kecil, dan menengah dalam upaya pencarian sumber akses permodalan yang
                            lebih mudah dan terjangkau.
                        </p>
                    </header>

                    <p class="text-center">
                        <h4><b>Tim Kami</b></h4>
                    </p>

                    <div class="row">
                        <div class="col-lg-2 col-md-6 wow fadeInUp">
                            <div class="member">
                                <img src="img/team-1.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>Walter White</h4>
                                        <span>Head of
                                            Investor Organize</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 wow fadeInUp">
                            <div class="member">
                                <img src="img/team-2.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>Walter White</h4>
                                        <span>Chief Executive Officer</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 wow fadeInUp">
                            <div class="member">
                                <img src="img/team-3.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>Walter White</h4>
                                        <span>Chief Executive Officer</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="member">
                                <img src="img/team-4.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>Sarah Jhonson</h4>
                                        <span>Product Manager</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="member">
                                <img src="img/team-5.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>William Anderson</h4>
                                        <span>CTO</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                            <div class="member">
                                <img src="img/team-6.jpg" class="img-fluid" alt="">
                                <div class="member-info">
                                    <div class="member-info-content">
                                        <h4>Imanuel Pay</h4>
                                        <span>Head of
                                            Technology</span>
                                        <div class="social">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row tentang-extra">
                        <div class="col-lg-6 wow fadeInUp">
                            <img src="img/about-extra-1.svg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
                            <p>
                                Senggang.com didirikan oleh orang-orang yang telah berpengalaman dalam dunia finansial,
                                bisnis, investasi, dan teknologi. Dengan visi "Menjadi platform lokal nasional yang
                                memberdayakan dan mengembangkan antara Pelaku Usaha Kecil Menengah Lokal dengan
                                Investor Lokal pada 2025 di Indonesia.".
                            </p>
                            <p>
                                Senggang.com berharap mampu mendorong pengembangan UKM di Indonesia yang menguntungkan
                                bagi pelaku usaha dan kumpulan pemberi pinjaman yang mendukungnya.
                            </p>
                            <p>
                                Dengan semangat ini kami berharap dapat memberikan dampak sosial seperti pertumbuhan
                                ekonomi, hingga secara tidak langsung membangun ekosistem kewirausahaan yang sehat dan
                                membuka lapangan pekerjaan melalui pemberdayaan ukm yang ada.
                            </p>
                        </div>
                    </div>

                    <div class="row tentang-extra">
                        <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
                            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
                        </div>

                        <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
                            <p>
                                Senggang.com menyediakan akses permodalan kepada pelaku usaha dengan menjual lembar
                                kepemilikan saham usaha mereka kepada publik (Investor Retail Lokal Nasional).
                                Senggang.com juga membuka alternatif peluang pemberian dana investasi kepada masyarakat
                                Indonesia.
                            </p>
                            <p>
                                Senggang.com membantu pelaku usaha untuk mengembangkan dan mematangkan bisnisnya serta
                                mempersiapkannya untuk memperoleh pendanaan dari investor retail maupun perusahaan agar
                                dapat mengembangkan bisnisnya dan mensejahterakan komunitasnya.
                            </p>
                            <p>
                                Para investor merupakan malaikat penolong yang tentunya akan memperoleh imbal hasil yang
                                sudah ditentukan oleh tim melalui penilaian yang telah kami lakukan.
                            </p>
                        </div>

                    </div>

                </div>
            </section>
            <!-- #tentang -->

            <!--==========================
      Contact Section
    ============================-->
            <section id="contact">
                <div class="container-fluid">

                    <div class="section-header">
                        <h3>Kontak Kami</h3>
                    </div>

                    <div class="row wow fadeInUp">
                        <div class="col-lg-2">

                        </div>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-md-5 info">
                                    <i class="ion-ios-location-outline"></i>
                                    <p>Jl. Ruba Dara</p>
                                </div>
                                <div class="col-md-4 info">
                                    <i class="ion-ios-email-outline"></i>
                                    <p>immanuelpay@gmail.com</p>
                                </div>
                                <div class="col-md-3 info">
                                    <i class="ion-ios-telephone-outline"></i>
                                    <p>+62 800 0000 0000</p>
                                </div>
                            </div>

                            <div class="form">
                                <div id="sendmessage">Your message has been sent. Thank you!</div>
                                <div id="errormessage"></div>
                                <form action="" method="post" role="form" class="contactForm">
                                    <div class="form-row">
                                        <div class="form-group col-lg-6">
                                            <input type="text" name="name" class="form-control" id="name"
                                                placeholder="Nama Anda" data-rule="minlen:4"
                                                data-msg="Please enter at least 4 chars" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <input type="email" class="form-control" name="email" id="email"
                                                placeholder="Email Anda" data-rule="email"
                                                data-msg="Please enter a valid email" />
                                            <div class="validation"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="subject" id="subject"
                                            placeholder="Judul" data-rule="minlen:4"
                                            data-msg="Please enter at least 8 chars of subject" />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" rows="5" data-rule="required"
                                            data-msg="Please write something for us" placeholder="Pesan"></textarea>
                                        <div class="validation"></div>
                                    </div>
                                    <div class="text-center"><button type="submit" title="Send Message">Kirim
                                            Pesan</button></div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
            <!-- #contact -->

        </main>

        <!--==========================
    Footer
  ============================-->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Link Informasi</h4>
                            <ul>
                                <li><a href="#">Beranda</a></li>
                                <li><a href="#">Tentang Kami</a></li>
                                <li><a href="#">Kampanye</a></li>
                                <li><a href="#">Kontak Kami</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-contact">
                            <h4>Kontak Kami</h4>
                            <p>
                                Jl. Ruba Dara <br> Batuplat, Alak<br> Kota Kupang <br>
                                <strong>Phone:</strong> +62 800 0000 0000<br>
                                <strong>Email:</strong> immanuelpay@gmail.com<br>
                            </p>

                            <div class="social-links">
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            </div>

                        </div>

                        <div class="col-lg-5 col-md-6 footer-info">
                            <h3>Senggang.com</h3>
                            <p>
                                Senggang.com adalah platform asal Provinsi Nusa Tenggara Timur yang memfasilitasi
                                investor dan pelaku usaha untuk saling bahu membahu menyelesaikan permasalahannya.
                            </p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="copyright">
                    &copy; Copyright <strong>Senggang.com</strong>. All Rights Reserved
                </div>
                <div class="credits">

                    Dibuat oleh <a href="https://facebook.com/immanuelpay">iMMANUEL PAY</a>
                </div>
            </div>
        </footer>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <script src="lib/jquery/jquery.min.js"></script>
        <script src="lib/jquery/jquery-migrate.min.js"></script>
        <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/mobile-nav/mobile-nav.js"></script>
        <script src="lib/wow/wow.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/counterup/counterup.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>
        <script src="lib/lightbox/js/lightbox.min.js"></script>
        <!-- Contact Form JavaScript File -->
        <script src="contactform/contactform.js"></script>

        <!-- Template Main Javascript File -->
        <script src="js/main.js"></script>

    </body>

</html>