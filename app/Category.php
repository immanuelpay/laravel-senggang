<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parentID', 'id');
    }

    public function child()
    {
    	return $this->hasMany('App\Category', 'parentID');
    }

    public function product()
    {
        return $this->hasMany('App\Product', 'categoryID');
    }

    public function scopeGetParent($query)
    {
    	return $this->whereNull('parentID');
    }

    public function scopeGetActive($query)
    {
    	return $this->where('status', 'ACTIVE');
    }

    public function scopeGetChild($query)
    {
        return $this->whereNotNull('parentID');
    }

    public function setSlugAttribute($value)
    {
    	$this->attributes['slug'] = Str::slug($value);
    }

    public function getNameAttribute($value)
    {
    	return ucfirst($value);
    }
}
