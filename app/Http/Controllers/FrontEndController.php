<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'DESC')->get();
        $categories = Category::getParent()->orderBy('name', 'ASC')->get();

        return view('front.home', compact('products', 'categories'));
    }
}
