<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;
use App\User;
use App\UserAttribute;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    // Administrator Controller
    public function administratorIndex()
    {
        if (request()->ajax()) {
            return datatables()->of(User::where('roles', 'ADMIN')->get())
                ->editColumn('roles', function ($request) {
                    if ($request->roles == 'ADMIN') {
                        return 'Administrator';
                    }
                })
                ->addColumn('action', function ($request) {
                    return view('admin.users.action', [
                        'request' => $request,
                        'url_show' => route('user.administrator.show', Crypt::encrypt($request->id)),
                        'url_edit' => route('user.administrator.edit', Crypt::encrypt($request->id)),
                        'url_delete' => route('user.administrator.delete', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.users.administrator.index');
    }

    public function administratorCreate()
    {
        return view('admin.users.administrator.create');
    }

    public function administratorStore(Request $request)
    {
        $administrator = new User;
        $administrator->name = $request->get('name');
        $administrator->username = $request->get('username');
        $administrator->email = $request->get('email');
        $administrator->password = bcrypt($request->get('password'));
        $administrator->status = $request->get('status');
        $administrator->roles = 'ADMIN';

        if ($request->file('avatar')) {
            $path_image = public_path('images/user');
            if (!File::isDirectory($path_image)) {
                File::makeDirectory($path_image, 0777, true, true);
            }

            $image_tmp = $request->file('avatar');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($administrator->name, '-') . '.' . $extension;
                $medium_image_path = $path_image . '/' . $fileName;

                Image::make($image_tmp)->orientate()->resize(600, 600)->save($medium_image_path);
                $administrator->avatar = $fileName;
            }
        }
        $administrator->save();

        $attribute = new UserAttribute;
        $attribute->user_id = $administrator->id;
        $attribute->gender = $request->get('gender');
        $attribute->dob = $request->get('dob');
        $attribute->address = $request->get('address');
        $attribute->phone = $request->get('phone');
        $attribute->facebook = $request->get('facebook');
        $attribute->instagram = $request->get('instagram');
        $attribute->twitter = $request->get('twitter');
        $attribute->save();

        toast()->success('Administrator : ' . $administrator->name . ', successfully saved.');
        return redirect()->route('user.administrator.index');
    }

    public function administratorShow($id)
    {
        $administrator = User::findOrFail(Crypt::decrypt($id));
        $attribute = UserAttribute::where('user_id', $administrator->id)->first();
        $dob = Carbon::parse($attribute->dob)->format('l, d F Y');

        return view('admin.users.administrator.show', compact('administrator', 'attribute', 'dob'));
    }

    public function administratorEdit($id)
    {
        $administrator = User::findOrFail(Crypt::decrypt($id));
        $attribute = UserAttribute::where('user_id', $administrator->id)->first();
        $dob = Carbon::parse($attribute->dob)->format('Y-m-d');

        return view('admin.users.administrator.edit', compact('administrator', 'attribute', 'dob'));
    }

    public function administratorUpdate(Request $request, $id)
    {
        $administrator = User::findOrFail($id);
        $administrator->name = $request->get('name');
        $administrator->username = $request->get('username');
        $administrator->email = $request->get('email');

        if ($request->get('password') != '') {
            $administrator->password = bcrypt($request->get('password'));
        }

        $administrator->status = $request->get('status');

        if ($request->file('avatar')) {
            $path_image = public_path('images/user');
            if (!File::isDirectory($path_image)) {
                File::makeDirectory($path_image, 0777, true, true);
            }

            if ($administrator->avatar != $request->get('avatar') && $administrator->avatar != 'avatar.png') {
                File::delete('images/user/' . $administrator->avatar);
            }

            $image_tmp = $request->file('avatar');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($administrator->name, '-') . '.' . $extension;
                $medium_image_path = $path_image . '/' . $fileName;

                Image::make($image_tmp)->orientate()->resize(600, 600)->save($medium_image_path);
                $administrator->avatar = $fileName;
            }
        }

        $administrator->update();

        $attribute = UserAttribute::where('user_id', $administrator->id)->first();
        $attribute->user_id = $administrator->id;
        $attribute->gender = $request->get('gender');
        $attribute->dob = $request->get('dob');
        $attribute->address = $request->get('address');
        $attribute->phone = $request->get('phone');
        $attribute->facebook = $request->get('facebook');
        $attribute->instagram = $request->get('instagram');
        $attribute->twitter = $request->get('twitter');
        $attribute->update();

        toast()->success('Administrator : ' . $administrator->name . ', successfully changed.');
        return redirect()->route('user.administrator.index');
    }

    public function administratorDelete($id)
    {
        $administrator = User::findOrFail(Crypt::decrypt($id));

        if ($administrator->id == auth()->user()->id) {
            toast()->error('Administrator : ' . $administrator->name . ', cannot deleted! You are Loged in with this user.');
            return back();
        } else {
            if ($administrator->avatar != 'avatar.png') {
                File::delete('images/user/' . $administrator->avatar);
            }

            $administrator->delete();
            toast()->success('Administrator : ' . $administrator->name . ', successfully deleted.');
            return redirect()->route('user.administrator.index');
        }
    }


    // Supplier Controller
    public function supplierIndex()
    {
        if (request()->ajax()) {
            return datatables()->of(User::where('roles', 'SUPPLIER')->get())
                ->addColumn('companyName', function ($request) {
                    $company = Supplier::where('userID', $request->id)->first();
                    return $company->companyName;
                })
                ->editColumn('roles', function ($request) {
                    if ($request->roles == 'SUPPLIER') {
                        return 'Supplier';
                    }
                })
                ->addColumn('action', function ($request) {
                    return view('admin.users.action', [
                        'request' => $request,
                        'url_show' => route('user.supplier.show', Crypt::encrypt($request->id)),
                        'url_edit' => route('user.supplier.edit', Crypt::encrypt($request->id)),
                        'url_delete' => route('user.supplier.delete', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.users.supplier.index');
    }

    public function supplierCreate()
    {
        return view('admin.users.supplier.create');
    }

    public function supplierStore(Request $request)
    {
        $supplier = new User;
        $supplier->name = $request->get('name');
        $supplier->username = $request->get('username');
        $supplier->email = $request->get('email');
        $supplier->password = bcrypt($request->get('password'));
        $supplier->roles = 'SUPPLIER';

        if ($request->file('avatar')) {
            $path_image = public_path('images/user/supplier');
            if (!File::isDirectory($path_image)) {
                File::makeDirectory($path_image, 0777, true, true);
            }

            $image_tmp = $request->file('avatar');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($supplier->name, '-') . '.' . $extension;
                $medium_image_path = $path_image . '/' . $fileName;

                Image::make($image_tmp)->orientate()->resize(600, 600)->save($medium_image_path);
                $supplier->avatar = $fileName;
            }
        }
        $supplier->save();

        $attribute = new UserAttribute;
        $attribute->user_id = $supplier->id;
        $attribute->gender = $request->get('gender');
        $attribute->dob = $request->get('dob');
        $attribute->address = $request->get('address');
        $attribute->phone = $request->get('phone');
        $attribute->facebook = $request->get('facebook');
        $attribute->instagram = $request->get('instagram');
        $attribute->twitter = $request->get('twitter');
        $attribute->save();

        $info = new Supplier;
        $info->userID = $supplier->id;
        $info->companyName = $request->get('companyName');
        $info->email =  $request->get('companyEmail');
        $info->save();

        toast()->success('Supplier : ' . $supplier->name . ', successfully saved.');
        return redirect()->route('user.supplier.index');
    }

    public function supplierShow($id)
    {
        $supplier = User::findOrFail(Crypt::decrypt($id));
        $company = Supplier::where('userID', $supplier->id)->first();
        $attribute = UserAttribute::where('user_id', $supplier->id)->first();

        return ['supplier_account' => $supplier, 'supplier_info' => $attribute, 'supplier_company' => $company];
    }

    public function supplierEdit($id)
    {
        $supplier = User::findOrFail(Crypt::decrypt($id));
        $company = Supplier::where('userID', $supplier->id)->first();
        $attribute = UserAttribute::where('user_id', $supplier->id)->first();
        $dob = Carbon::parse($attribute->dob)->format('Y-m-d');

        return view('admin.users.supplier.edit', compact('supplier', 'company', 'attribute', 'dob'));
    }

    public function supplierUpdate(Request $request, $id)
    {
        //
    }

    public function supplierDelete($id)
    {
        //
    }


    // Customer Controller
    public function customerIndex()
    {
        return view('admin.users.customer.index');
    }

    public function customerCreate()
    {
        return view('admin.users.customer.create');
    }

    public function customerStore(Request $request)
    {
        //
    }

    public function customerShow($id)
    {
        //
    }

    public function customerEdit($id)
    {
        //
    }

    public function customerUpdate(Request $request, $id)
    {
        //
    }

    public function customerDelete($id)
    {
        //
    }
}
