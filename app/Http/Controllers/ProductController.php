<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('admin_supplier');

        // if (Gate::allows('admin')) {
        if (request()->ajax()) {
            return datatables()->of(Product::with(['category'])->get())
                ->editColumn('categoryID', function ($request) {
                    return $request->category->name;
                })
                ->editColumn('price', function ($request) {
                    return 'IDR ' . number_format($request->price, 2);
                })
                ->addColumn('action', function ($request) {
                    return view('admin.products.action', [
                        'request' => $request,
                        'url_show' => route('products.show', Crypt::encrypt($request->id)),
                        'url_edit' => route('products.edit', Crypt::encrypt($request->id)),
                        'url_delete' => route('products.delete', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        // }

        // if (Gate::allows('supplier')) {
        //     if (request()->ajax()) {
        //         return datatables()->of(Product::where('created_by', auth()->user()->id))
        //             ->editColumn('categoryID', function ($request) {
        //                 $category = Category::where('id', $request->categoryID)->first();
        //                 return $category->name;
        //             })
        //             ->editColumn('price', function ($request) {
        //                 return 'IDR ' . number_format($request->price, 2);
        //             })
        //             ->addColumn('action', function ($request) {
        //                 return view('admin.products.action', [
        //                     'request' => $request,
        //                     'url_show' => route('products.show', Crypt::encrypt($request->id)),
        //                     'url_edit' => route('products.edit', Crypt::encrypt($request->id)),
        //                     'url_delete' => route('products.delete', Crypt::encrypt($request->id)),
        //                 ]);
        //             })
        //             ->addIndexColumn()
        //             ->rawColumns(['action'])
        //             ->make(true);
        //     }
        // }

        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('admin_supplier');

        if (Category::getChild()->count() == 0 && Category::getActive()->count() == 0) {
            toast()->error('Please added a subcategory!');
            return redirect()->back();
        }

        return view('admin.products.create', [
            'categories' => Category::getChild()->orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('admin_supplier');

        $product = new Product;
        $product->title = $request->get('title');
        $product->categoryID = $request->get('category');
        $product->slug = Str::slug($product->title, '-');
        $product->code = Carbon::now()->timestamp . '_' . uniqid() . '_' . rand(111111111, 999999999);
        $product->price = $request->get('price');
        $product->sku = $request->get('sku');
        $product->profit = $request->get('profit');
        $product->stock = $request->get('stock');
        $product->short_summary = $request->get('short_summary');
        $product->detail = $request->get('detail');
        $product->description = $request->get('description');
        $product->status = $request->get('status');
        $product->created_by = auth()->user()->id;

        if ($request->hasFile('images')) {
            $path_original = public_path('images/product/original/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
            $path_medium = public_path('images/product/medium/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
            $path_small = public_path('images/product/small/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);

            if (!File::isDirectory($path_original) && !File::isDirectory($path_medium) && !File::isDirectory($path_small)) {
                File::makeDirectory($path_original, 0777, true, true);
                File::makeDirectory($path_medium, 0777, true, true);
                File::makeDirectory($path_small, 0777, true, true);
            }

            foreach ($request->file('images') as $image_tmp) {
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . $product->slug . '.' . $extension;
                    $original_image_path = $path_original . '/' . $fileName;
                    $medium_image_path = $path_medium . '/' . $fileName;
                    $small_image_path = $path_small . '/' . $fileName;

                    Image::make($image_tmp)->orientate()->save($original_image_path);
                    Image::make($image_tmp)->orientate()->resize(800, 800)->save($medium_image_path);
                    Image::make($image_tmp)->orientate()->resize(400, 400)->save($small_image_path);

                    $images[] = $fileName;
                }
            }

            $product->images = json_encode($images);
        }

        $product->save();

        toast()->success('Product : ' . $product->title . ', successfully saved.');
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $this->authorize('admin_supplier');

        $product = Product::findOrFail(Crypt::decrypt($id));
        $category = Category::where('id', $product->categoryID)->first();
        $user = User::where('id', $product->created_by)->first();
        $path_medium = 'images/product/medium/' . $user->id . '_' . $user->username . '/' . '._' . $product->code;
        return view('admin.products.show', compact('product', 'category', 'path_medium', 'user'));
    }

    public function downloadImage($slug)
    {
        // $product = Product::where('created_by', auth()->user()->id)->first();
        // if ($product->slug == $slug) {
        //     $path = asset('images/product/medium/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
        //     $images = json_decode($product->images);
        //     foreach ($images as $image) {
        //         echo ($path . '/' . $image . ' ');
        //     }
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $this->authorize('admin_supplier');

        return view('admin.products.edit', ['product' => Product::findOrFail(Crypt::decrypt($id)), 'categories' => Category::getChild()->orderBy('name', 'ASC')->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->authorize('admin_supplier');

        $product = Product::findOrFail($id);
        $product->title = $request->get('title');
        $product->slug = Str::slug($product->title, '-');
        $product->categoryID = $request->get('category');
        $product->price = $request->get('price');
        $product->profit = $request->get('profit');
        $product->stock = $request->get('stock');
        $product->sku = $request->get('sku');
        $product->short_summary = $request->get('short_summary');
        $product->detail = $request->get('detail');
        $product->description = $request->get('description');
        $product->status = $request->get('status');
        $product->updated_by = auth()->user()->id;

        if ($request->hasFile('images')) {
            $path_original = public_path('images/product/original/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
            $path_medium = public_path('images/product/medium/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
            $path_small = public_path('images/product/small/' . auth()->user()->id . '_' . auth()->user()->username . '/' . '._' . $product->code);
            if (File::isDirectory($path_original) && File::isDirectory($path_medium) && File::isDirectory($path_small)) {
                File::deleteDirectory($path_original);
                File::deleteDirectory($path_medium);
                File::deleteDirectory($path_small);
            }

            if (!File::isDirectory($path_original) && !File::isDirectory($path_medium) && !File::isDirectory($path_small)) {
                File::makeDirectory($path_original, 0777, true, true);
                File::makeDirectory($path_medium, 0777, true, true);
                File::makeDirectory($path_small, 0777, true, true);
            }

            foreach ($request->file('images') as $image_tmp) {
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . $product->slug . '.' . $extension;
                    $original_image_path = $path_original . '/' . $fileName;
                    $medium_image_path = $path_medium . '/' . $fileName;
                    $small_image_path = $path_small . '/' . $fileName;

                    Image::make($image_tmp)->orientate()->save($original_image_path);
                    Image::make($image_tmp)->orientate()->resize(800, 800)->save($medium_image_path);
                    Image::make($image_tmp)->orientate()->resize(400, 400)->save($small_image_path);

                    $images[] = $fileName;
                }
            }

            $product->images = json_encode($images);
        }

        $product->update();

        toast()->success('Product : ' . $product->title . ', successfully changed.');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // $this->authorize('admin_supplier');

        $product = Product::findOrFail(Crypt::decrypt($id));
        $product->deleted_by = auth()->user()->id;
        $product->update();
        $product->delete();

        toast()->success('Product : ' . $product->title . ', successfully moved to trash.');
        return redirect()->route('products.index');
    }

    public function trash()
    {
        // $this->authorize('admin_supplier');

        // if (Gate::allows('admin')) {
        if (request()->ajax()) {
            return datatables()->of(Product::onlyTrashed()->get())
                ->editColumn('categoryID', function ($request) {
                    return $request->category->name;
                })
                ->editColumn('price', function ($request) {
                    return 'IDR ' . number_format($request->price, 2);
                })
                ->addColumn('action', function ($request) {
                    return view('admin.products.action', [
                        'request' => $request,
                        'url_restore' => route('products.restore', Crypt::encrypt($request->id)),
                        'url_delete_permanent' => route('products.delete.permanent', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        // }

        // if (Gate::allows('supplier')) {
        //     if (request()->ajax()) {
        //         return datatables()->of(Product::where('created_by', auth()->user()->id)->onlyTrashed()->get())
        //             ->editColumn('categoryID', function ($request) {
        //                 $category = Category::where('id', $request->categoryID)->first();
        //                 return $category->name;
        //             })
        //             ->editColumn('price', function ($request) {
        //                 return 'IDR ' . number_format($request->price, 2);
        //             })
        //             ->addColumn('action', function ($request) {
        //                 return view('admin.products.action', [
        //                     'request' => $request,
        //                     'url_restore' => route('products.restore', Crypt::encrypt($request->id)),
        //                     'url_delete_permanent' => route('products.delete.permanent', Crypt::encrypt($request->id)),
        //                 ]);
        //             })
        //             ->addIndexColumn()
        //             ->rawColumns(['action'])
        //             ->make(true);
        //     }
        // }

        return view('admin.products.trash');
    }

    public function restore($id)
    {
        // $this->authorize('admin_supplier');

        $product = Product::withTrashed()->findOrFail(Crypt::decrypt($id));
        if ($product->trashed()) {
            $product->restore();
        } else {
            toast()->error('Product : ' . $product->title . ', is not in trash.');
            return redirect()->route('products.index');
        }

        toast()->success('Product : ' . $product->title . ', successfully restored.');
        return redirect()->route('products.index');
    }

    public function deletePermanent($id)
    {
        // $this->authorize('admin_supplier');

        $product = Product::withTrashed()->findOrFail(Crypt::decrypt($id));
        $user = User::where('id', $product->created_by)->first();

        if (!$product->trashed()) {
            toast()->error('Product : ' . $product->title . ', Can not delete permanent active category.');
            return redirect()->route('products.index');
        } else {
            $path_original = public_path('images/product/original/' . $user->id . '_' . $user->username . '/' . '._' . $product->code);
            $path_medium = public_path('images/product/medium/' . $user->id . '_' . $user->username . '/' . '._' . $product->code);
            $path_small = public_path('images/product/small/' . $user->id . '_' . $user->username . '/' . '._' . $product->code);
            if (File::isDirectory($path_original) && File::isDirectory($path_medium) && File::isDirectory($path_small)) {
                File::deleteDirectory($path_original);
                File::deleteDirectory($path_medium);
                File::deleteDirectory($path_small);
            }
            $product->forceDelete();

            toast()->success('Product : ' . $product->title . ', permanently deleted.');
            return redirect()->route('products.index');
        }
    }
}
