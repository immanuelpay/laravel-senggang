<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\User;
use App\UserAttribute;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::all()->count();
        $parent = Category::getParent()->count();
        $subcategory = Category::getChild()->count();
        return view('home', compact('product', 'parent', 'subcategory'));
    }

    public function profile()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $attribute = UserAttribute::where('user_id', auth()->user()->id)->first();
        $dob = Carbon::parse($attribute->dob)->format('l, d F Y');

        return view('admin.profile', compact('user', 'attribute', 'dob'));
    }

    public function profileEdit(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = User::where('id', auth()->user()->id)->first();
            $user->name = $request->get('name');
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            if ($request->file('avatar')) {
                $path_image = public_path('images/user');
                if (!File::isDirectory($path_image)) {
                    File::makeDirectory($path_image, 0777, true, true);
                }

                if ($user->avatar != $request->get('avatar') && $user->avatar != 'avatar.png') {
                    File::delete('images/user/' . $user->avatar);
                }

                $image_tmp = $request->file('avatar');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($user->name, '-') . '.' . $extension;
                    $medium_image_path = $path_image . '/' . $fileName;

                    Image::make($image_tmp)->orientate()->resize(600, 600)->save($medium_image_path);
                    $user->avatar = $fileName;
                }
            }

            $user->update();

            $attributes = UserAttribute::where('user_id', auth()->user()->id)->get();
            if (isset($attributes)) {
                $attribute = UserAttribute::where('user_id', auth()->user()->id)->first();
                $attribute->gender = $request->get('gender');
                $attribute->dob = $request->get('dob');
                $attribute->address = $request->get('address');
                $attribute->phone = $request->get('phone');
                $attribute->facebook = $request->get('facebook');
                $attribute->instagram = $request->get('instagram');
                $attribute->twitter = $request->get('twitter');
                $attribute->update();
            } else {
                toast()->error('Data not found.');
                return back();
            }

            toast()->success('Data successfully added.');
            return back();
        }

        $user = User::where('id', auth()->user()->id)->first();
        $attribute = UserAttribute::where('user_id', auth()->user()->id)->first();
        $dob = Carbon::parse($attribute->dob)->format('Y-m-d');

        return view('admin.profile_setting', compact('user', 'attribute', 'dob'));
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);

        $data = $request->all();
        $user = User::find(auth()->user()->id);
        if (!Hash::check($data['old_password'], $user->password)) {
            toast()->error('Please try again!', 'Your current password does not matches with the password you provided. ');
            return redirect()->back();
        } else {
            $user->update(['password' => bcrypt($request->new_password)]);
            toast()->success('Your successfully changed password.');
            return redirect()->back();
        }
    }

    public function inactive()
    {
        $user = User::find(auth()->user()->id);
        $user->status = 'INACTIVE';
        $user->update();

        Auth::logout();
        toast()->success('Your successfully deactivated username.');
        return redirect()->route('login');
    }

    public function delete()
    {
        $attribute = UserAttribute::where('user_id', auth()->user()->id)->first();
        $attribute->delete();

        $user = User::find(auth()->user()->id);
        $user->delete();

        Auth::logout();
        toast()->success('Your successfully deleted username.');
        return redirect()->route('login');
    }
}
