<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('admin_supplier');

        if (request()->ajax()) {
            return datatables()->of(Category::all())
                ->editColumn('parentID', function ($request) {
                    if ($request->parentID == null) {
                        return '-';
                    } else {
                        return $request->parent->name;
                    }
                })
                ->addColumn('action', function ($request) {
                    return view('admin.categories.action', [
                        'request' => $request,
                        'url_edit' => route('categories.edit', Crypt::encrypt($request->id)),
                        'url_delete' => route('categories.delete', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('admin');

        return view('admin.categories.create', [
            'parents' => Category::getParent()->orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('admin');

        $this->validate($request, [
            'name' => 'required|min:3|max:20',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,ico|max:2048',
        ]);

        $category = new Category;
        $category->name = $request->get('name');
        $category->parentID = $request->get('parent');
        $category->slug = Str::slug($category->name, '-');
        $category->created_by = auth()->user()->id;

        if ($request->hasFile('image')) {
            $path_medium = public_path('images/category/medium/');
            $path_small = public_path('images/category/small/');
            if (!File::isDirectory($path_medium) && !File::isDirectory($path_small)) {
                File::makeDirectory($path_medium, 0777, true, true);
                File::makeDirectory($path_small, 0777, true, true);
            }

            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($category->name, '-') . '.' . $extension;
                $medium_image_path = $path_medium . '/' . $fileName;
                $small_image_path = $path_small . '/' . $fileName;

                Image::make($image_tmp)->orientate()->resize(800, 800)->save($medium_image_path);
                Image::make($image_tmp)->orientate()->resize(400, 400)->save($small_image_path);

                $category->image = $fileName;
            }
        }

        $category->save();

        toast()->success('Category : ' . $category->name . ', successfully saved.');
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Category::findOrFail(Crypt::decrypt($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $this->authorize('admin');

        return view('admin.categories.edit', [
            'category' => Category::findOrFail(Crypt::decrypt($id)),
            'parents' => Category::getParent()->orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->authorize('admin');

        $category = Category::findOrFail($id);
        $category->name = $request->get('name');
        $category->parentID = $request->get('parent');
        $category->slug = Str::slug($category->name, '-');
        $category->updated_by = auth()->user()->id;

        if ($category->deleted_by != "") {
            $category->deleted_by = null;
        }

        if ($request->hasFile('image')) {
            $path_medium = public_path('images/category/medium/');
            $path_small = public_path('images/category/small/');
            if (!File::isDirectory($path_medium) && !File::isDirectory($path_small)) {
                File::makeDirectory($path_medium, 0777, true, true);
                File::makeDirectory($path_small, 0777, true, true);
            }

            if ($category->image != $request->get('image')) {
                File::delete('images/category/medium/' . $category->image);
                File::delete('images/category/small/' . $category->image);
            }

            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = Carbon::now()->timestamp . '_' . uniqid() . '_' . Str::slug($category->name, '-') . '.' . $extension;
                $medium_image_path = $path_medium . '/' . $fileName;
                $small_image_path = $path_small . '/' . $fileName;

                Image::make($image_tmp)->orientate()->resize(800, 800)->save($medium_image_path);
                Image::make($image_tmp)->orientate()->resize(400, 400)->save($small_image_path);

                $category->image = $fileName;
            }
        }

        $category->update();

        toast()->success('Category : ' . $category->name . ', successfully changed.');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // $this->authorize('admin');

        $category = Category::withCount(['child', 'product'])->find(Crypt::decrypt($id));

        if ($category->child_count == 0 && $category->product_count == 0) {
            $category->deleted_by = auth()->user()->id;
            $category->update();
            $category->delete();

            toast()->success('Category : ' . $category->name . ', successfully moved to trash.');
            return redirect()->route('categories.index');
        }

        toast()->error('Category : ' . $category->name . ', error moved to trash. This category have child or product');
        return redirect()->route('categories.index');
    }

    public function trash()
    {
        // $this->authorize('admin');

        if (request()->ajax()) {
            return datatables()->of(Category::onlyTrashed()->get())
                ->editColumn('parentID', function ($request) {
                    if ($request->parentID == null) {
                        return 'No Parent';
                    } else {
                        return $request->parent->name;
                    }
                })
                ->addColumn('action', function ($request) {
                    return view('admin.categories.action', [
                        'request' => $request,
                        'url_restore' => route('categories.restore', Crypt::encrypt($request->id)),
                        'url_delete_permanent' => route('categories.delete.permanent', Crypt::encrypt($request->id)),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.categories.trash');
    }

    public function restore($id)
    {
        // $this->authorize('admin');

        $category = Category::withTrashed()->findOrFail(Crypt::decrypt($id));
        if ($category->trashed()) {
            $category->restore();
        } else {
            toast()->error('Category : ' . $category->name . ', is not in trash.');
            return redirect()->route('categories.index');
        }

        toast()->success('Category : ' . $category->name . ', successfully restored.');
        return redirect()->route('categories.index');
    }

    public function deletePermanent($id)
    {
        // $this->authorize('admin');

        $category = Category::withTrashed()->findOrFail(Crypt::decrypt($id));
        if (!$category->trashed()) {
            toast()->error('Category : ' . $category->name . ', Can not delete permanent active category.');
            return redirect()->route('categories.index');
        } else {
            File::delete('images/category/medium/' . $category->image);
            File::delete('images/category/small/' . $category->image);
            $category->forceDelete();

            toast()->success('Category : ' . $category->name . ', permanently deleted.');
            return redirect()->route('categories.index');
        }
    }
}
