<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('gender')->nullable();
            $table->timestamp('dob')->nullable();
            $table->longText('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->longText('otherAttribute')->nullable();

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('userID')->nullable();
            $table->string('class')->nullable();
            $table->string('room')->nullable();
            $table->string('building')->nullable();
            $table->text('addres1')->nullable();
            $table->text('addres2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('postalCode')->nullable();
            $table->string('country')->nullable();
            $table->string('creditCard')->nullable();
            $table->string('creditCardTypeID')->nullable();
            $table->string('cardExpMo')->nullable();
            $table->string('cardExpYr')->nullable();
            $table->text('billingAddress')->nullable();
            $table->text('billingCity')->nullable();
            $table->text('billingRegion')->nullable();
            $table->text('billingPostalCode')->nullable();
            $table->text('billingCountry')->nullable();
            $table->text('shipAddress')->nullable();
            $table->text('shipCity')->nullable();
            $table->text('shipRegion')->nullable();
            $table->text('shipPostalCode')->nullable();
            $table->text('shipCountry')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('image5')->nullable();
            $table->string('image6')->nullable();
            $table->string('image7')->nullable();

            $table->timestamps();
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('userID')->nullable();
            $table->string('companyName')->nullable();
            $table->string('contactFName')->nullable();
            $table->string('contactLName')->nullable();
            $table->string('contactTitle')->nullable();
            $table->text('addres1')->nullable();
            $table->text('addres2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('postalCode')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('website')->nullable();
            $table->string('paymentMethods')->nullable();
            $table->string('discountType')->nullable();
            $table->string('discountRate')->nullable();
            $table->string('typeGoods')->nullable();
            $table->boolean('discountAvailable')->nullable();
            $table->boolean('currentOrder')->nullable();
            $table->string('sizeURL')->nullable();
            $table->string('colorURL')->nullable();
            $table->string('picture')->default('picture.png');
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('image5')->nullable();
            $table->string('image6')->nullable();
            $table->string('image7')->nullable();
            $table->integer('rangking')->unsigned()->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_attributes');
    }
}
