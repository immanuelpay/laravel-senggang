<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('categoryID');
            $table->string('code')->unique();
            $table->string('sku')->unique();
            $table->string('title');
            $table->string('slug');
            $table->float('price', 20, 2);
            $table->integer('profit')->nullable();
            $table->integer('stock')->default(0);
            $table->longText('images');
            $table->text('short_summary');
            $table->longText('detail');
            $table->longText('description');
            $table->enum('status', ['ENABLE', 'DISABLE']);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('categoryID')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
