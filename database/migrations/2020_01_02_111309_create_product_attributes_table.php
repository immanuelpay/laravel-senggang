<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->unsigned();
            $table->string('availableSize')->nullable();
            $table->string('availableColors')->nullable();
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('quantityPerStock')->nullable();
            $table->integer('stockWeight')->nullable();
            $table->integer('stockOnOrder')->nullable();
            $table->string('productAvailable')->nullable();
            $table->string('discountAvailable')->nullable();
            $table->string('rangking')->nullable();

            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attributes');
    }
}
