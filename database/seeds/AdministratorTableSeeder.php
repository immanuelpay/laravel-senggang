<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdministratorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
        $administrator->username = "administrator";
        $administrator->name = "Administrator";
        $administrator->email = "admin@admin.com";
        $administrator->roles = 'ADMIN';
        $administrator->is_main_admin = true;
        $administrator->status = 'ACTIVE';
        $administrator->password = Hash::make("password");
        $administrator->save();

        $attribute = new \App\UserAttribute;
        $attribute->user_id = $administrator->id;
        $attribute->save();

        $this->command->info("User Admin successfully saved!");
    }
}
