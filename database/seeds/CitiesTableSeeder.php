<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url_city = "https://api.rajaongkir.com/starter/city?key=11ac03c5b141fdaf5064510024c1e407";
        $json_str = file_get_contents($url_city);
        $json_obj = json_decode($json_str);
        $cities = [];
        foreach ($json_obj->rajaongkir->results as $city) {
            $cities[] = [
                'id'            => $city->city_id,
                'province_id'   => $city->province_id,
                'province'      => $city->province,
                'type'          => $city->type,
                'city_name'     => $city->city_name,
                'postal_code'   => $city->postal_code,
            ];
        }
        DB::table('cities')->insert($cities);
    }
}
