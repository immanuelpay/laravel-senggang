<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/customer', 'FrontEndController@index')->name('front.index');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('profile', 'HomeController@profile')->name('profile');
Route::match(['get', 'post'], 'profile-setting', 'HomeController@profileEdit')->name('profile.setting');
Route::get('profile-inactive', 'HomeController@inactive')->name('profile.inactive');
Route::get('profile-delete', 'HomeController@delete')->name('profile.delete');
Route::post('change-password', 'HomeController@changePassword')->name('change-password');

Route::get('categories/{id}/delete', 'CategoryController@delete')->name('categories.delete');
Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::get('categories/{id}/delete-permanent', 'CategoryController@deletePermanent')->name('categories.delete.permanent');
Route::resource('categories', 'CategoryController');

Route::get('products/{id}/delete', 'ProductController@delete')->name('products.delete');
Route::get('products/trash', 'ProductController@trash')->name('products.trash');
Route::get('products/{id}/restore', 'ProductController@restore')->name('products.restore');
Route::get('products/{id}/delete-permanent', 'ProductController@deletePermanent')->name('products.delete.permanent');
Route::get('products/{slug}/download-image', 'ProductController@downloadImage')->name('products.download.image');
Route::resource('products', 'ProductController');

Route::group(['prefix' => 'users'], function () {
    Route::get('administrator', 'UserController@administratorIndex')->name('user.administrator.index');
    Route::get('administrator/create', 'UserController@administratorCreate')->name('user.administrator.create');
    Route::post('administrator', 'UserController@administratorStore')->name('user.administrator.store');
    Route::get('administrator/{id}', 'UserController@administratorShow')->name('user.administrator.show');
    Route::get('administrator/{id}/edit', 'UserController@administratorEdit')->name('user.administrator.edit');
    Route::match(['put', 'patch'], 'administrator/{id}', 'UserController@administratorUpdate')->name('user.administrator.update');
    Route::get('administrator/{id}/delete', 'UserController@administratorDelete')->name('user.administrator.delete');

    Route::get('supplier', 'UserController@supplierIndex')->name('user.supplier.index');
    Route::get('supplier/create', 'UserController@supplierCreate')->name('user.supplier.create');
    Route::post('supplier', 'UserController@supplierStore')->name('user.supplier.store');
    Route::get('supplier/{id}', 'UserController@supplierShow')->name('user.supplier.show');
    Route::get('supplier/{id}/edit', 'UserController@supplierEdit')->name('user.supplier.edit');
    Route::match(['put', 'patch'], 'supplier/{id}', 'UserController@supplierUpdate')->name('user.supplier.update');
    Route::get('supplier/{id}/delete', 'UserController@supplierDelete')->name('user.supplier.delete');

    Route::get('customer', 'UserController@customerIndex')->name('user.customer.index');
    Route::get('customer/create', 'UserController@customerCreate')->name('user.customer.create');
    Route::post('customer', 'UserController@customerStore')->name('user.customer.store');
    Route::get('customer/{id}', 'UserController@customerShow')->name('user.customer.show');
    Route::get('customer/{id}/edit', 'UserController@customerEdit')->name('user.customer.edit');
    Route::match(['put', 'patch'], 'customer/{id}', 'UserController@customerUpdate')->name('user.customer.update');
    Route::get('customer/{id}/delete', 'UserController@customerDelete')->name('user.customer.delete');
});
